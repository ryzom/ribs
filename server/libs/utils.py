#############################################
# | ___ \|_   _|| ___ \/  ___|
# | |_/ /  | |  | |_/ /\ `--.
# |    /   | |  | ___ \ `--. \
# | |\ \ __| |__| |_/ //\__/ /
# \_| \_(_)___(_)____(_)____(_)
#
# Remote Interface to Bash Scripts
# - better with barcebue sauce -
# Copyright (C) 2019 Nuneo (ulukyn@gmail.com)
#
# This program is free software (GPLv3): read https://www.gnu.org/licenses/gpl-3.0.en.html for more details
#
# Utils functions
#

from colorama import Fore, Back, Style
import base64

def decode_base64(data):
	"""Decode base64, padding being optional.

	:param data: Base64 data as an ASCII byte string
	:returns: The decoded byte string.

	"""
	missing_padding = len(data) % 4
	if missing_padding:
		data += "="* (4 - missing_padding)
	return base64.urlsafe_b64decode(data)

def encode(key, clear):
	enc = []
	for i in range(len(clear)):
		key_c = key[i % len(key)]
		enc_c = chr((ord(clear[i]) + ord(key_c)) % 256)
		enc.append(enc_c)
	return base64.urlsafe_b64encode("".join(enc).encode()).decode()

def decode(key, enc):
	dec = []
	enc = decode_base64(enc).decode()
	for i in range(len(enc)):
		key_c = key[i % len(key)]
		dec_c = chr((256 + ord(enc[i]) - ord(key_c)) % 256)
		dec.append(dec_c)
	return "".join(dec)

def p(text, fg="", bg="", style=""):

	try:
		fg = getattr(Fore, fg.upper())
	except AttributeError:
		fg = ""

	try:
		bg = getattr(Back, bg.upper())
	except AttributeError:
		bg = ""

	try:
		style = getattr(Style, style.upper())
	except AttributeError:
		style = ""

	print(fg + bg + style + text + Style.RESET_ALL)


def pp(text, fg="", bg="", style=""):

	try:
		fg = getattr(Fore, fg.upper())
	except AttributeError:
		fg = ""

	try:
		bg = getattr(Back, bg.upper())
	except AttributeError:
		bg = ""

	try:
		style = getattr(Style, style.upper())
	except AttributeError:
		style = ""

	print(fg + bg + style + text + Style.RESET_ALL, end="")
