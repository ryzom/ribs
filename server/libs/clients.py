#############################################
# | ___ \|_   _|| ___ \/  ___|
# | |_/ /  | |  | |_/ /\ `--.
# |    /   | |  | ___ \ `--. \
# | |\ \ __| |__| |_/ //\__/ /
# \_| \_(_)___(_)____(_)____(_)
#
# Remote Interface to Bash Scripts
# - better with barcebue sauce -
# Copyright (C) 2019 Nuneo (ulukyn@gmail.com)
#
# This program is free software (GPLv3): read https://www.gnu.org/licenses/gpl-3.0.en.html for more details
#
# Client class
#

import os, json, uuid
from libs.utils import p


class Client():
	"""Client class store the session using flat files in server side.
	"""
	uid = ""
	orderer_id = 0
	session = {}
	app = None

	def __init__(self, uid, app):
		"""Init the instance by loading the file who contain
		saved session values.
		"""
		self.uid = uid
		self.app = app
		if uid == 1:
			self.session= {"groups" : ":dev:"}
			return
		if not self.session:
			if os.path.isfile(self.app.data_path+"/sessions/" + uid.hex):
				self.session = json.loads(
					open(self.app.data_path+"/sessions/" + uid.hex, "r").read())
			else:
				self.session = {}

	def getSession(self, var):
		"""Return the value stored in session."""
		if var in self.session:
			return str(self.session[var])
		return ""

	def setSession(self, var, value):
		"""Set a value to store in session.
		File is saved immediately."""
		self.session[var] = value
		open(self.app.data_path+"/sessions/" + self.uid.hex, "w").write(
			json.dumps(self.session))

	def deleteSession(self):
		"""Delete the session file and erase session content"""
		if os.path.isfile(self.app.data_path+"/sessions/" + self.uid.hex):
			os.remove(self.app.data_path+"/sessions/" + self.uid.hex)
		self.session = {}

	def getConfig(self, var):
		"""Return the value of an app config parameter.
		Parameteres are managed by Flask. This method just read them.
		"""
		if var in self.app.config:
			return self.app.config[var]
		return ""


class ClientManager():
	"""Manage the Clients by the uid provided by flask.
	Create a new Client when need.
	"""
	clients = {}

	def __init__(self, app, sessions):
		"""Initialize with Flask app and sessions."""
		self.app = app
		self.sessions = sessions

	def getCurrentClientId(self, uid=None):
		"""Return the uid of the current client using flask sessions."""
		if uid:
			return uid

		if "uid" in self.sessions:
			uid = self.sessions["uid"]
			p("Client %s is back!" % uid, "yellow")
		else:
			uid = uuid.uuid4()
			p("New Client. Welcome %s!" % uid, "cyan")
			self.sessions["uid"] = uid

		return uid

	def getCurrentClient(self, uid=None):
		"""Return the current client using flask sessions."""
		uid = self.getCurrentClientId(uid)
		if not uid in self.clients:
			self.clients[uid] = Client(uid, self.app)

		return self.clients[uid]

	def delCurrentClient(self):
		"""Delete the current client."""
		uid = self.getCurrentClientId()
		if uid in self.clients:
			self.clients[uid].deleteSession()
			del(self.clients[uid])

	def getClientSession(self, uid, var):
		"""Wrapper to getSession method of Client."""
		if uid in self.clients:
			return self.clients[uid].getSession(var)
		return None

	def setClientSession(self, uid, var, value):
		"""Wrapper to setSession method of Client."""
		if uid in self.clients:
			return self.clients[uid].setSession(var, value)
		return None
