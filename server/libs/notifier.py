#############################################
# | ___ \|_   _|| ___ \/  ___|
# | |_/ /  | |  | |_/ /\ `--.
# |    /   | |  | ___ \ `--. \
# | |\ \ __| |__| |_/ //\__/ /
# \_| \_(_)___(_)____(_)____(_)
#
# Remote Interface to Bash Scripts
# - better with barcebue sauce -
# Copyright (C) 2019 Nuneo (ulukyn@gmail.com)
#
# This program is free software (GPLv3): read https://www.gnu.org/licenses/gpl-3.0.en.html for more details
#
# Notifier class
#

import sys, gc, datetime
import gevent
from gevent.queue import Queue
from json import dumps
from time import time, gmtime, strftime
from libs.utils import p
from libs.cooker import RibsCooker

class Notifier():
	"""Send messages to Web Clients.
	Keep a list of Cookings and Listeners to know where send the messages.
	"""

	messages = {} # List of queues used to sent messages
	cookers = {} # Map of Cookers
	listeners = {"*" : []} # List of Listener who want receives message about cooking, * are all listeners

	last_sent_message = {}
	logs = []
	cooking_logs = {}

	def __init__(self, filterMessages):
		"""Init and set filterMessages function."""
		self.filterMessages = filterMessages
		self.messages["logs"] = Queue() # Used to sent logs to listeners


	def addListener(self, listener_id):
		"""Add a message queue for listerner."""
		if not listener_id in self.messages:
			self.messages[listener_id] = Queue()
		if not listener_id in self.listeners["*"]:
			self.listeners["*"].append(listener_id)

	def removeListener(self, listener_id):
		"""Remove a Listener."""
		del self.messages[listener_id]
		self.listeners["*"].remove(listener_id)
		for cooking in self.listeners:
			if listener_id in self.listeners[cooking]:
				self.listeners[cooking].remove(listener_id)

	def addCooker(self, name, cooker):
		"""Keep a link to the cooker."""
		p("New Cooker %s ready!" % name , "yellow")
		self.cookers[name] = cooker

	def removeCooker(self, name,):
		"""Remove the cooker."""
		p("Remove the Cooker %s" % name , "yellow")
		if name in self.cookers:
			del self.cookers[name]

	def addCookerListener(self, listener_id, cooking):
		"""Add a Listener of Cooker messages."""
		self.addListener(listener_id)
		# add the listener to cooker queue
		if not cooking in self.listeners:
			self.listeners[cooking] = []
		if not listener_id in self.listeners[cooking]:
			p("New Listerner '%s' for Cooking '%s'" % (listener_id, cooking), "yellow")
			self.listeners[cooking].append(listener_id)

	def getCookingLog(self, cooking):
		if cooking in Notifier.cooking_logs:
			return Notifier.cooking_logs[cooking]
		return ""

	def getLogs(self, count):
		return "<br />".join(Notifier.logs[-count:])

	def timeoutListeners(self):
		"""Remove Listeners who have stop to receive at least messages."""
		current_time = time()
		for listener in self.listeners["*"].copy():
			if listener in self.last_sent_message:
				if current_time - self.last_sent_message[listener] > 9:
					p("Listener %s have timeout..." % listener, "red")
					del self.last_sent_message[listener]
					self.removeListener(listener)
					# Delete cooker
					RibsCooker(listener, None, None)

	def sendMessageToListener(self, listener_id, cooker, ribs, msg, event):
		"""Send a message to a Listener and only them."""
		self.timeoutListeners()

		def notify( msg):
			if listener_id in self.messages:
				self.messages[listener_id].put((cooker, ribs, event, msg))

		if isinstance(msg, str):
			gevent.spawn(notify, msg.replace("\n", ""))
		else:
			gevent.spawn(notify, msg)

	def eventMessageForListeners(self, cooker, ribs, msg, event):
		"""Send a message to all Listeners."""
		#print("sendMessageToListeners", "["+cooking+"/"+ribs+"]", msg, event, self.listener_cookings)
		for listener in self.listeners["*"]:
			self.sendMessageToListener(listener, cooker, ribs, msg, event)

	def eventMessageFromCooker(self, cooker, ribs, msg, event):
		"""Send a message to all Listeners of a Cooking."""
		#print("sendMessageToCookingListeners", "["+cooking+"/"+ribs+"]", msg, event, self.listener_cookings)
		def notify(msg):
			if cooker == "kitchen":
				p("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"+ribs+" : "+event, "red")
			if cooker in self.listeners:
				for listener in self.listeners[cooker]:
					self.sendMessageToListener(listener, cooker, ribs, msg, event)
		if isinstance(msg, str):
			gevent.spawn(notify, msg.replace("\n", ""))
		else:
			gevent.spawn(notify, msg)

	def emitToListener(self, listener_id):
		"""Send a message to Listener by looking the queue in a loop."""
		block = ""
		is_block = False
		while True:
			if listener_id in self.messages:
				cooker, filteredMessages = self.filterMessages(self, listener_id, self.messages[listener_id].get())
				#if cooking != "Ping":
				#	print("emitToListener", cooking, filteredMessages)
				for message in filteredMessages:
					(event, result) = message

					self.last_sent_message[listener_id] = time()

					# End of block. Send all block content to listeners
					if event == "end_block":
						is_block = False
						result = block
						event = "output"

					# Output in a block, save the content for later
					elif is_block and event == "output":
						block = block + result
						continue

					# Start of block or in a block, save the content for later
					elif event == "start_block":
						block = ""
						is_block = True
						continue

					if event == "exit":
						print("unsubscribed")
						return

					if event:
						yield "event: %s\n" % event

					yield "data: %s\n\n" % dumps((cooker, result))

	def eventStartCooking(self, name):
		"""A cooking has started."""
		p("New Cooking %s started!" % name , "yellow")
		self.eventMessageForListeners(name, name, name, "newCook")


	def eventStopCooking(self, name):
		"""A cooking has stopped."""
		p("Cooker of Orderer not cooking. Deleted them", "red")
		self.eventMessageForListeners(name, name, name, "delCook")
#		cooker.stopWorking()
#		del self.cookings[name]

	def eventLog(self, log, cooking=""):
		Notifier.logs.append(log)
		if cooking:
			Notifier.cooking_logs[cooking] = log
