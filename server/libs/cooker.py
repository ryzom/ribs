#############################################
# | ___ \|_   _|| ___ \/  ___|
# | |_/ /  | |  | |_/ /\ `--.
# |    /   | |  | ___ \ `--. \
# | |\ \ __| |__| |_/ //\__/ /
# \_| \_(_)___(_)____(_)____(_)
#
# Remote Interface to Bash Scripts
# - better with barcebue sauce -
# Copyright (C) 2019 Nuneo (ulukyn@gmail.com)
#
# This program is free software (GPLv3): read https://www.gnu.org/licenses/gpl-3.0.en.html for more details
#
# Cooker class
#

import os
import sys
import gc
import signal
import gevent
import psutil
import traceback

from json import dumps
from time import localtime, strftime, sleep, time
from gevent.subprocess import Popen, PIPE
from gevent.lock import BoundedSemaphore
from gevent.queue import Queue
from libs.utils import p


def RibsCookerMultiton(cls):
	instances = {}
	def getinstance(id=None, user=None, notifier=None):
		if not id:
			return instances

		if id == "get_cookers":
			cookers = {}
			for name, i in instances.items():
				print(name, end="")
				if i.is_cooking:
					cookers[i.id] = i.owner

			return cookers

		if not user and not notifier:
			if id in instances:
				print("delete %s" % id)
				del instances[id]
			return

		if id not in instances:
			instances[id] = cls(id, user, notifier)
		return instances[id]
	return getinstance

###################################
# Ribs Cooker Class
@RibsCookerMultiton
class RibsCooker():

	def __init__(self, id, user, notifier):
		self.id = id
		self.user = user
		self.owner = user.getSession("username")
		if not self.owner:
			self.owner = "Auto-Ribs"
		self.orderer_id = user.orderer_id
		self.cookings = []
		self.cooking_process = None
		self.cooking_rib = ""
		self.cooking_rib_name = ""
		self.notifier = notifier
		self.waiting_ribs = Queue()
		self.started = False
		self.is_cooking = False
		self.cooking_ping = False
		self.semaphore = BoundedSemaphore(1)

		notifier.addCooker(id, self)

		p("Added Cooker %s" % id, "cyan")


	def isCookingPing(self, rib):
		return self.cooking_ping


	def isCooking(self):
		return self.is_cooking


	def isStarted(self):
		return self.started


	def emitToMainListener(self, ribs, message, event="output"):
		"""Send a message to the main Listener of this Cooker."""
		self.notifier.sendMessageToListener(self.orderer_id, "kitchen", ribs, message, event)


	def emitToAllListeners(self, ribs, message, event="output"):
		"""Send a message to all Listeners of this Cooker."""
		if self.id == self.orderer_id:
			return self.emitToMainListener(ribs, message, event)

		self.notifier.eventMessageForListeners(self.id, ribs, message, event)

		if ribs != "000_init.sh" and event in ("running", "finished", "stopped"):
			timer = strftime("%d/%m %H:%M:%S", localtime())
			log = "<span class='time'>%s</span> " % timer
			log += "<span class='user'>%s</span> " % self.owner
			if event == "running":
				log += "<span class='event-%s'>%s</span> %s" % (event, event, message[0]+" "+" ".join(message[1]))
			else:
				log += "<span class='event-%s'>%s</span> %s" % (event, event, message[0])
			self.notifier.eventMessageForListeners("logs", ribs, log, "logs")

			if event == "running":
				self.notifier.eventLog(log, self.id)
			elif event == "finished":
				self.notifier.eventLog(log)
			else:
				self.notifier.eventLog(log)


	def startWorking(self):
		"""Start the Cooker, will wait for ribs who need be cooked."""
		p("Start the Cooker '%s'!" % self.id, "cyan")
		self.started = True
		while self.started:
			ribs, path, options = self.waiting_ribs.get()
			if ribs:
				p("Getting new ribs '%s' of cooking '%s' to cook!" % (ribs, self.id), "yellow")
				self.cook(ribs, path, options)
				self.semaphore.acquire()
				self.semaphore.release()
				p("End cook...", "green")
			else:
				break
		p("Waiting Stop of Cooker '%s'" % self.id, "red")
		self.semaphore.acquire()
		p("Stopped Working of Cooker '%s'" % self.id, "red")
		self.semaphore.release()
		self.notifier.eventStopCooking(self.id)
		RibsCooker(self.id, None, None)


	def stopWorking(self):
		"""Stop the Cooker."""
		p("Stop the Cooker '%s'!" % self.id, "red")
		self.started = False
		self.cooking_ping = False
		# Force startWorking to update and stop
		self.waiting_ribs.put(("", "", ""))
		p("done")


	def stopCooking(self):
		p("Empty queue")
		self.waiting_ribs = Queue()


	def cookPing(self, ribs, path, params):
		self.cooking_ping = True
		while self.cooking_ping:
			timer  = strftime("%d/%m %H:%M:%S", localtime())
			self.emitToMainListener(ribs, (timer, RibsCooker("get_cookers")), "ping")
			gevent.sleep(3)
		p("Stopped Cooking Ping of '%s'" % self.id, "red")


	def generateEnv(self):
		my_env = os.environ.copy()
		my_env["RIBS_BASE_PATH"] = self.user.getConfig("BASEDIR")
		my_env["WEB_AUTH_SALT"] = self.user.getConfig("SECRET_KEY")
		my_env["RIBS_USER_PATH"] = self.user.getConfig("USER_PATH")
		my_env["RIBS_USER_SCRIPT_PATH"] = self.user.getConfig("USER_SCRIPTS_PATH")
		my_env["RIBS_SCRIPT_PATH"] = os.getcwd() + "/scripts"
		my_env["RIBS_CONTEXT"] = "WEB"
		my_env["RIBS_USER"] = str(self.user.getSession("username"))
		my_env["RIBS_GROUPS"] = str(self.user.getSession("groups"))
		my_env["RIBS_FLAVOUR"] = self.user.getConfig("FLAVOUR")

		return my_env



	def cookRib(self, ribs, path="USER_SCRIPTS_PATH", params=[]):

		self.semaphore.acquire()
		p("Cooking rib '%s' of cooking '%s'" % (ribs, self.id), "cyan")
		my_env = self.generateEnv()

		if params and params[0] == "Run":
			event = "running"
		else:
			event = "prepare"

		self.notifier.eventStartCooking(self.id)
		self.is_cooking = True
		self.cooking_rib_name = ribs.replace("/", "__")
		self.cooking_rib = self.user.getConfig(path) + "/" + ribs
		rib_command = ["scripts/tools/screen_ribs.sh", self.cooking_rib_name, self.cooking_rib] + params
		p("Running command [%s]" % " ".join(rib_command), "yellow")
		rib_process = Popen(rib_command, stdout=PIPE, stderr=PIPE, env=my_env)
		self.cooking_process = rib_process

		# Init script don't need be broadcasted.
		if ribs != "000_init.sh" :
			if event == "running":
				self.emitToAllListeners(ribs, (ribs, params, self.owner), event)
			else:
				self.emitToMainListener(ribs, (ribs, params, self.owner), event)

		def readStdout():
			current_time = time()
			temp_chunk = []
			while True:  # Read output process..
				chunk = str(rib_process.stdout.readline(), "utf-8", "ignore")
				# print(chunk)
				if chunk and (chunk[0] != "(" or chunk[1] != ">"):
					if time() - current_time < 0.01:
						temp_chunk.append(chunk)
						current_time = time()
						continue
				current_time = time()

				if chunk or temp_chunk:
					if ribs == "000_init.sh":
						if temp_chunk:
							self.emitToMainListener(ribs, "<br />".join(temp_chunk))
							temp_chunk = []
						self.emitToMainListener(ribs, chunk)
					else:
						if temp_chunk:
							self.emitToAllListeners(ribs, "<br />".join(temp_chunk))
							temp_chunk = []
						self.emitToAllListeners(ribs, chunk)
						sleep(0.01) # temp fix
				else:
					rib_process.stdout.close()
					p("End of process %s!" % ribs, "green")
					self.cooking_process = None
					self.is_cooking = False
					break

		def readStderr():
			current_time = time()
			temp_chunk = ""
			while True:  # Read error process..
				if time() - current_time < 0.01:
					try:
						chunk = str(rib_process.stderr.readline(), "utf-8")
					except:
						chunk = ""
					if chunk:
						temp_chunk += chunk
						if chunk[0] != ">":
							temp_chunk += "<br />"
					continue
				else:
					chunk = temp_chunk
					temp_chunk = ""
					current_time = time()

				if chunk or temp_chunk:
					if ribs == "000_init.sh":
						self.emitToMainListener(ribs, chunk, "error")
					else:
						self.emitToAllListeners(ribs, chunk, "error")
				else:
					p("End of stderr %s!" % ribs, "green")
					# Stderr close always after stdout
					if event == "prepare":
						self.emitToMainListener(ribs, (ribs, self.id, self.owner), "prepared")
					else:
						self.emitToAllListeners(ribs, (ribs, self.id ,self.owner), "finished")
					self.semaphore.release()
					break

		gevent.spawn(readStdout);
		gevent.spawn(readStderr);



	def cook(self, ribs, path="", params=[]):
		if ribs == "Ping":
			gevent.spawn(self.cookPing, ribs, path, params)
		else:
			self.cookRib(ribs, path, params)

	def wantCook(self, ribs, path, options):
		if self.is_cooking:
			p("Cooking allready started !", "red")
			self.emitToMainListener(self.id+" are allready cooking...", "error")
			return
		p("Adding ribs '%s' of cooker '%s' to queue" % (ribs, self.id), "cyan")
		self.waiting_ribs.put((ribs, path, options))


	def stopCook(self):
		if self.is_cooking:
			my_env = self.generateEnv()
			self.stopCooking()
			self.stopWorking()
			stop_command = ["scripts/tools/stop_cooking.sh", self.cooking_rib, self.cooking_rib_name]
			rib_process = Popen(stop_command, env=my_env)
			return
