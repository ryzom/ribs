#############################################
# | ___ \|_   _|| ___ \/  ___|
# | |_/ /  | |  | |_/ /\ `--.
# |    /   | |  | ___ \ `--. \
# | |\ \ __| |__| |_/ //\__/ /
# \_| \_(_)___(_)____(_)____(_)
#
# Remote Interface to Bash Scripts
# - better with barcebue sauce -
# Copyright (C) 2019 Nuneo (ulukyn@gmail.com)
#
# This program is free software (GPLv3): read https://www.gnu.org/licenses/gpl-3.0.en.html for more details
#
# Ribs config file
#

# User config vars setup
SERVER_HOST=localhost
SERVER_PORT=8081
RIBS_FOLDER=ribs
RIBS_GROUPS=:dev:
RIBS_SECRET_KEY="RibsIsGood"

# SSL stuff. Start ribs with an user in ssl-cert group
RIBS_SSL_PATH=""
AUTORIBS_FILE=""
#AUTORIBS_FILE="/home/nevrax/www/public_html/autoribs_hook.cmd"



# Env var setup (don't need any changes)
BASEDIR="$(realpath $(dirname "$(readlink -f "$0")")/..)"
export RIBS_BASE_PATH=$BASEDIR
export RIBS_USER_PATH=$BASEDIR/$RIBS_FOLDER
export RIBS_SCRIPT_PATH=$BASEDIR/server/scripts/
export RIBS_USER_SCRIPT_PATH=$RIBS_USER_PATH/scripts
export RIBS_DATA_PATH=$BASEDIR/data
export RIBS_GROUPS
export RIBS_SECRET_KEY
export RIBS_SSL_PATH
export AUTORIBS_FILE
export BASEDIR

