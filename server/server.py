#!/usr/bin/env python
#############################################
# | ___ \|_   _|| ___ \/  ___|
# | |_/ /  | |  | |_/ /\ `--.
# |    /   | |  | ___ \ `--. \
# | |\ \ __| |__| |_/ //\__/ /
# \_| \_(_)___(_)____(_)____(_)
#
# Remote Interface to Bash Scripts
# - better with barcebue sauce -
# Copyright (C) 2019 Nuneo (ulukyn@gmail.com)
#
# This program is free software (GPLv3): read https://www.gnu.org/licenses/gpl-3.0.en.html for more details
#
# Server main script
#

import os, sys, json, re
import shlex
import gevent
import base64
import subprocess

from colorama import Fore, Style
from simplecrypt import encrypt

from gevent.pywsgi import WSGIServer
import flask

from libs.cooker import RibsCooker
from libs.clients import Client, ClientManager
from libs.notifier import Notifier
from libs.utils import p, pp, encode


# Read config from config.sh file
pp("Reading config env vars... ")
command = shlex.split("env -i bash -c 'source config.sh && env'")
proc = subprocess.Popen(command, stdout = subprocess.PIPE)
for line in proc.stdout:
  (key, _, value) = line.decode("utf-8").partition("=")
  os.environ[key] = value.replace("\n", "").replace("\r", "")
proc.communicate()
p("Done", "green")

app = flask.Flask(__name__)
app.secret_key = os.environ["RIBS_SECRET_KEY"]
app.testing = False
app.config["DEBUG"] = True
app.config["SCRIPTS_PATH"] = "scripts/" # Server scripts path
app.config["SECRET_KEY"] = app.secret_key
app.config["BASEDIR"] =  os.environ["BASEDIR"]

APPNAME = "Ryzom R.I.B.S"
VERSION = "0.9.7"
AUTHOR = "Nuneo"
DATE = "2019"

##################################
### Utilities ####################


##################################
# FilterMessages
def filterMessages(self, listener_id, params):
	"""Filter message using listener_id and params.
	Send message if need using self who are the Notifier."""
	(cooking, ribs, event, res) = params
	final = []
	temp = ""
	if not isinstance(res, str):
		final.append((event, res))
		return (cooking, final)

	progress = re.search("\[([ 0-9][ 0-9][0-9])%\](.*)", res)
	if progress:
		return (cooking, [("progress", progress.group(1)+"|"+progress.group(2))]);
	elif res[:2] == "(>":
		sres = res[2:].split(")")
		if len(res) >= 2:
			action = sres[0]
			value = ")".join(sres[1:])
		elif len(res) == 1:
			action = sres[0]
			value = ""
		else:
			return (cooking, final)

		p("(>"+" "+action+" = "+value, "white")
		# auth system need a json array with:
		#	"success", "username" and "groups" fields

		if action == "login":
			try:
				login_values = json.loads(value)
			except Exception as e:
				p(e, "red")
				login_values = {}
			print("auth", login_values)
			if "success" in login_values and login_values["success"]:
				print(">>>>>>>>>>>>>", listener_id, self.cookers)
				if listener_id in self.cookers:
					p("ICI", "green")
					cooker = self.cookers[listener_id]
					cooker.user.setSession("username", login_values["username"])
					cooker.user.setSession("groups", login_values["groups"])
				print("sent auth to %s", listener_id)
				self.sendMessageToListener(listener_id, cooking, ribs, login_values["username"], "logged")
				self.sendMessageToListener(listener_id, cooking, ribs, listener_id, "refresh")
				final.append((event, temp))
				temp = ""
			else:
				self.sendMessageToListener(listener_id, cooking, ribs, "Auth failed...", "error")

		elif action == "logged":
			self.sendMessageToListener(listener_id, cooking, ribs, value, "logged")

		elif action == "select":  #this is a select list, send event ask
			final.append((event, temp))
			final.append(("list", ribs + "|" + value))
			temp = ""

		elif action == "input":  #this is a input text, send event ask
			final.append((event, temp))
			final.append(("inputText", ribs + "|" + value))
			temp = ""

		elif action == "password":  #this is a input text, send event ask
			final.append((event, temp))
			final.append(("inputPassword", ribs + "|" + value))
			temp = ""

		elif action == "ask":
			final.append((event, temp))
			final.append(("ask", ribs + "|" + value))
			temp = ""

		elif action == "text":
			final.append((event, temp))
			final.append((action, value))
			temp = ""

		elif action == "refresh" or  action == "reprepare":
			final.append((event, temp))
			final.append((action, listener_id))
			temp = ""

		elif action == "fail":
			#client = client_manager.getCurrentClient()
			cooker = RibsCooker(cooking, "client", notifier)
			if cooker.isCooking():
				cooker.stopCook()
			final.append(("stopped", temp))
			temp = ""

		else:
			final.append((event, temp))
			final.append((action, ribs + "|" + value))
			temp = ""
	else:
		temp += res

	if temp:
		final.append((event, temp))

	return (cooking, final)


##################################
### Flask Routes #################


##################################
# Logout
@app.route("/logout/<orderer_id>")
def logout(orderer_id):
	client = client_manager.getCurrentClient()
	if not client.session:
		return "no soup for you"
	
	client_manager.delCurrentClient()
	notifier.sendMessageToListener(orderer_id, "kitchen", "", orderer_id, "refresh")
	return "OK"



##################################
# Index
@app.route("/")
def home():
	server_name = flask.request.host_url.split(":")[1][2:].upper()
	header = flask.render_template(
		"header.html", app_name=APPNAME, server_name=server_name)
	bottom = flask.render_template(
		"bottom.html", version=VERSION, author=AUTHOR, date=DATE)
	return flask.render_template("index.html", header=header, bottom=bottom)



##################################
# Stop
@app.route("/stop/<orderer_id>/<cooker>")
def stop(orderer_id, cooker):
	client = client_manager.getCurrentClient()
	if not client.session:
		return "no soup for you"

	cooker = RibsCooker(cooker, client, notifier)
	cooker.emitToMainListener(
			"kitchen",
			"<span style='color: orange'>"
			"Asking process to stop. Please wait..."
			"</span>", "output")
	if cooker.isCooking():
		cooker.stopCook()
	return flask.Response("ok")



##################################
# Stream
@app.route("/stream/<orderer_id>")
def stream(orderer_id):

	# client is linked to flask session, don't change when page is refresh
	client = client_manager.getCurrentClient()


	client.orderer_id = orderer_id

	# The Cooker of orderer, only for him
	cooker = RibsCooker(orderer_id, client, notifier)

	# An Orderer are also a Listener of own Cooker. Make sense xD
	notifier.addCookerListener(orderer_id, cooker)

	# Start init script
	cooker.wantCook("000_init.sh", "SCRIPTS_PATH", [])


	# Ping is used to check if Orderer is allready listening.
	# Need be check each time the web client ask for the stream
	pp("Check if the Orderer have a Ping in the cooker... ", "green")
	if cooker.isCookingPing("Ping"):
		p("\tyes!", "blue")
	else:
		p("\tno! Starting it", "yellow")
		cooker.cook("Ping", "Ping")

	# Start the cooker if need
	if not cooker.isStarted():
		cooker.CookingProcessId = gevent.spawn(cooker.startWorking)

	return flask.Response(notifier.emitToListener(orderer_id), mimetype="text/event-stream")


##################################
# Follow
@app.route("/follow/<listener_id>/<cooker>")
def follow(listener_id, cooker):
	client = client_manager.getCurrentClient()
	if not client.session:
		return "no soup for you"

	if cooker == "kitchen":
		cooker = listener_id
	notifier.addCookerListener(listener_id, cooker)
	p(cooker+" : "+notifier.getCookingLog(cooker), "red")
	notifier.sendMessageToListener(listener_id, cooker, "", notifier.getCookingLog(cooker), "activity")

	if cooker == "logs": # If listener ask to follow logs, display the latest 100 lines
		notifier.sendMessageToListener(listener_id, "logs", "logs", notifier.getLogs(100), "logs")

	return flask.Response("ok")


##################################
# Cook
@app.route("/cook/<orderer_id>/<cooker>/<ribs>/<is_run>", methods=["GET"])
def cook(orderer_id, cooker, ribs, is_run):
	print(">>>Cook", cooker, ribs, is_run)

	client = client_manager.getCurrentClient()
	if not client.session and cooker != "-Auth":
		return "no soup for you"

	if is_run != "1": #if not run, it's preparation and notification are only for him
		cooker = orderer_id

	cooker = RibsCooker(cooker, client, notifier)
	notifier.addCookerListener(orderer_id, cooker)

	def cleanOptions(x):
		if not x:
			return ""
		if isinstance(x, list):
			if x[1] == 1:
				if "=" in x[0]:
					key, val = x[0].split("=", 2)
					key = key + "="
				else:
					key = ""
					val = x[0]
				return key+encode(app.secret_key, val)
			else:
				print(x[0])
				if "=" in x[0]:
					key, val = x[0].split("=", 1)
					return key+"="+shlex.quote(val)
				return shlex.quote(x[0])
		return shlex.quote(x)

	ribs = json.loads(base64.b64decode(ribs).decode("utf-8"))
	for rib in ribs:
		if rib[0] == "000_init.sh":
			path = "SCRIPTS_PATH"
		else:
			path = "USER_SCRIPTS_PATH"
		options = list(filter(None, map(cleanOptions, rib[1])))
		print("#################################")
		print(rib)
		print(options)
		print("#################################")
		cooker.wantCook(rib[0], path, options)

	# ask cooker to end
	cooker.wantCook("", "", "")

	if not cooker.isStarted():
		cooker.CookingProcessId = gevent.spawn(cooker.startWorking)

	return "OK"

def autoribs():
	client = client_manager.getCurrentClient(1)
	cooker = None
	with open(os.environ["AUTORIBS_FILE"], "r", encoding="utf-8", errors="replace") as f:
		p("\nAutoRibs enabled. Looking "+os.environ["AUTORIBS_FILE"]+"...", "cyan")
		f.seek(0,os.SEEK_END)
		while True:
			pos = f.tell()
			lines = f.readlines()
			if not lines:
				f.seek(pos)
				gevent.sleep(1)
			else:
				cookers = RibsCooker()
				while "Auto Ribs" in cookers:
					p("Auto Ribs working...", "red")
					gevent.sleep(5)

				cooker = RibsCooker("Auto Ribs", client, notifier)
				for line in lines:
					sline = line.replace("\n", "").split("|")
					cooker.wantCook(sline[0], "USER_SCRIPTS_PATH", sline[1:])
				cooker.wantCook("", "", "") # latest cook are for stop and delete the cooker
				gevent.spawn(cooker.startWorking)

pp("Creating ClientManager and Notifier instances... ")
client_manager = ClientManager(app, flask.session)
notifier = Notifier(filterMessages)
p("Done", "green")

if __name__ == "__main__":
	p("\n--- STARTING SERVER ---", "cyan")
	app.debug = False
	app.session = flask.session
	app.user_path = os.environ["RIBS_USER_PATH"]
	app.base_path = os.environ["RIBS_BASE_PATH"]
	app.data_path = os.environ["RIBS_DATA_PATH"]
	app.user_script_path = os.environ["RIBS_USER_SCRIPT_PATH"]

	full_address = sys.argv[2].split(":")
	address = full_address[0]
	port=8080
	if len(full_address) >= 2:
		port = int(full_address[1])

	app.config["USER_PATH"] = app.user_path
	app.config["USER_SCRIPTS_PATH"] = app.user_script_path

	if "AUTORIBS_FILE" in os.environ and os.path.isfile(os.environ["AUTORIBS_FILE"]):
		autoribs = gevent.spawn(autoribs)

	try:
		if os.environ["RIBS_SSL_PATH"]:
			server = WSGIServer((address, port),
				app,
				keyfile=os.environ["RIBS_SSL_PATH"]+"/privkey.pem",
				certfile=os.environ["RIBS_SSL_PATH"]+"/cert.pem")
		else:
			server = WSGIServer((address, port), app)
		p("\nRunning..."+str(server), "cyan")
		server.serve_forever()
	except KeyboardInterrupt as e:
		print("Error", str(e))



