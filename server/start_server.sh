#!/bin/bash
#############################################
# | ___ \|_   _|| ___ \/  ___|
# | |_/ /  | |  | |_/ /\ `--.
# |    /   | |  | ___ \ `--. \
# | |\ \ __| |__| |_/ //\__/ /
# \_| \_(_)___(_)____(_)____(_)
#
# Remote Interface to Bash Scripts
# - better with barcebue sauce -
# Copyright (C) 2019 Nuneo (ulukyn@gmail.com)
#
# This program is free software (GPLv3): read https://www.gnu.org/licenses/gpl-3.0.en.html for more details
#
# Start RIBS server
#

source config.sh

if [[ -z "$RIBS_SSL_PATH" ]]
then
	SERVER_HTTP="http"
else
	SERVER_HTTP="https"
fi

if [ ! -d $RIBS_DATA_PATH/sessions ]
then
	mkdir $RIBS_DATA_PATH/sessions
fi

echo "Starting R.I.B.S. server to $SERVER_HTTP://$SERVER_HOST:$SERVER_PORT"


env/bin/python server.py $RIBS_USER_PATH $SERVER_HOST:$SERVER_PORT $*
