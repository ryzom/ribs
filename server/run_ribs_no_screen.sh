#!/bin/bash
#############################################
# | ___ \|_   _|| ___ \/  ___|
# | |_/ /  | |  | |_/ /\ `--.
# |    /   | |  | ___ \ `--. \
# | |\ \ __| |__| |_/ //\__/ /
# \_| \_(_)___(_)____(_)____(_)
#
# Remote Interface to Bash Scripts
# - better with barcebue sauce -
# Copyright (C) 2019 Nuneo (ulukyn@gmail.com)
#
# This program is free software (GPLv3): read https://www.gnu.org/licenses/gpl-3.0.en.html for more details
#
# Run script to use ribs without web interface
#

BASEDIR="$(realpath $(dirname "$(readlink -f "$0")")/..)"

. $BASEDIR/server/scripts/tools/colors.sh
. $BASEDIR/server/config.sh

export RIBS_USER="cli"
export RIBS_GROUPS=":admin:dev:"

if [[ -z $1 ]]
then
	echo "run $RIBS_SCRIPT_PATH/000_Init.sh"
	bash $RIBS_SCRIPT_PATH/000_init.sh
else
	script=$1
	shift
	bash $RIBS_USER_SCRIPT_PATH/$script $*
fi
