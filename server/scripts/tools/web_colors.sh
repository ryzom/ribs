#!/bin/bash

function color {
	color=$1
	shift
	echo -n "<span style='color:$color'>$@</span>";
}

function clr_layer {
	color=$1
	shift
	echo "<span style='color:$color'>$@</span>";
}

function clr_start {
	color=$1
	echo "<div style='color:$color'>";
}

function clr_end {
	echo "</div>";
}

function clr_back {
	color=$1
	shift
	echo "<span style='background-color:$color'>$@</span>";
}

function input {
	echo "<input style='width:70%' type='text' value='$@' />";
}

function text {
	echo "<textarea style='width:70%; height:300px' >$@</textarea>";
}




function clr_bold            { echo "<strong>$@</strong>";			}
function clr_bright          { echo "$@";          }
function clr_underscore      { echo "$@";      }
function clr_reverse         { echo "$@";         }
function clr_black           { clr_layer black "$@";           }
function clr_red             { clr_layer red "$@";             }
function clr_green           { clr_layer lightgreen "$@";           }
function clr_brown           { clr_layer orange "$@";           }
function clr_blue            { clr_layer blue "$@";            }
function clr_magenta         { clr_layer magenta "$@";         }
function clr_cyan            { clr_layer cyan "$@";            }
function clr_white           { clr_layer white "$@";           }
function clr_blackb          { clr_back black "$@";          }
function clr_redb            { clr_back red "$@";            }
function clr_greenb          { clr_back lightgreen "$@";          }
function clr_brownb          { clr_back brown "$@"; }
function clr_blueb           { clr_back blue "$@";           }
function clr_magentab        { clr_back magenta "$@";        }
function clr_cyanb           { clr_back cyan "$@";           }
function clr_whiteb          { clr_back white "$@";          }
