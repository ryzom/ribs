#!/bin/bash

declare -A TOKEN

UC_SHARDNAME=$(hostname -s)

function send_chat {
	rocket_prefs="$RIBS_BASE_PATH/data/rocketchat_prefs.sh"
	if [[ ! -f $rocket_prefs ]]
	then
		echo "$2"
	else
		source $rocket_prefs
		var="$1"_TOKEN
		curl -s -X POST -H "Content-Type: application/json" --data "{\"text\":\"$2\", \"icon_emoji\": \"$4\", \"username\":\"$UC_SHARDNAME - R.I.B.S.\", \"attachments\":[{\"text\":\"$3\",\"color\":\"$5\"}]}" $RC_SERVER/hooks/${!var} > /dev/null
	fi
}
