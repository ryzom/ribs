#!/bin/bash

if [ "$RIBS_CONTEXT" == "WEB" ]
then
	. "$RIBS_SCRIPT_PATH/tools/web_colors.sh"
else
	. "$RIBS_SCRIPT_PATH/tools/colors.sh"
fi

declare -A OPTIONS
declare -A RECIPE_IDS

# if $1 not null, get all params from it
if [ ! -z "$1" ]
then
	for i in "$@"; do
		parameter=$(echo "$i" | cut -d"=" -f1)
		value=$(echo "$i" | cut -s -d"=" -f2-)
		if [ -z "$value" ]
		then
			parameter="$i"
			value="1"
		fi
		#echo "[$parameter]=$value"
		OPTIONS[$parameter]="$value"
	done
fi


function check_running_ribs() {
	RIBSNAME=$(echo $1 | sed 's#/#__#g')
	RET=$(screen -list | grep \\\.RIBS_$RIBSNAME | wc -l)
	if [[ "$RET" == "1" ]]
	then
		clr_red "RIBS $1 ALLREADY RUNNING! COOKING ABORTED"
	fi
}

function select_list() {
	if [ "$RIBS_CONTEXT" == "WEB" ]
	then
		echo "(>select)$1|$2|$3|$4|$5"
	else
		IFS=':' read -ra PARAMS <<< "$1"
		echo "0) ${PARAMS[1]}"
		IFS=':' read -ra PARAMS <<< "$2"
		echo "1) ${PARAMS[1]}"
		if [ ! -z "$3" ]
		then
			IFS=':' read -ra PARAMS <<< "$3"
			echo "2) ${PARAMS[1]}"
		fi
		if [ ! -z "$4" ]
		then
			IFS=':' read -ra PARAMS <<< "$4"
			echo "3) ${PARAMS[1]}"
		fi
		if [ ! -z "$5" ]
		then
			IFS=':' read -ra PARAMS <<< "$5"
			echo "4) ${PARAMS[1]}"
		fi

		args=("$@")
		read OPTION
		OPTION="${args[$OPTION]}"
		IFS=':' read -ra OPTION <<< "$OPTION"
		OPTION="${OPTION[0]}"
		echo $OPTION
		OPTIONS[$OPTION]="1"
	fi
}


function enter_text {
	if [ "$RIBS_CONTEXT" == "WEB" ]
	then
		echo "(>input)$1|$2"
	else
		read VALUE
		OPTIONS[$1]="$VALUE"
	fi
}

function enter_password {
	if [ "$RIBS_CONTEXT" == "WEB" ]
	then
		echo "(>password)$1"
	else
		read VALUE
		OPTIONS[$1]="$VALUE"
	fi
}

function script_name {
	echo $(basename $0 .sh)
}

function newline() {
	echo "(>text) "
}

function ask {
	if [ "$RIBS_CONTEXT" == "WEB" ]
	then
		#script_name=$(basename $0 .sh)
		echo "(>ask)$@"
	else
		clr_brown "$(clr_blackb $@)"
	fi
}

function clean {
	if [ "$RIBS_CONTEXT" == "WEB" ]
	then
		echo "(>clear) "
	fi
}

function refresh {
	if [ "$RIBS_CONTEXT" == "WEB" ]
	then
		echo "(>refresh) "
	fi
}

function reprepare {
	if [ "$RIBS_CONTEXT" == "WEB" ]
	then
		echo "(>reprepare) "
	fi
}


function login {
	if [ "$RIBS_CONTEXT" == "WEB" ]
	then
		echo "(>login)$1"
	fi
}

function display_user {
	user=$(clr_green "$RIBS_USER")
	groups=$(echo "$RIBS_GROUPS" | sed "s/:/ /g")
	clr_brown "=============================================================================================="
	clr_white "User:   " $user
	clr_white "Groups:" $(clr_green "$groups")
	clr_brown "=============================================================================================="
}

function logged {
	if [ "$RIBS_CONTEXT" == "WEB" ]
	then
		echo "(>logged)$1"
	fi
}

function ribs_script {
	str=""
	for i in "$@"; do
		str="$str,$i"
	done
	echo -n ${str:1}
}

function ribs_flavour {
	str=""
	for i in "$@"; do
		str="$str|$i"
	done
	echo "(>flavour)"${str:1}
}

function get_recipe {
	if [ "$RIBS_CONTEXT" == "WEB" ]
	then
		export RECIPE_NAME=$1
		clr_brown "Getting recipe $(clr_cyan ${1:0:-3})"
	fi
	. $1
}

function ribs_check {
	if [[ "$?" != "0" ]]
	then
		exit
	fi
}

function ribs_recipe {
	str=""
	for i in "$@"; do
		str="$str|$i"
	done
	if [ "$RIBS_CONTEXT" == "WEB" ]
	then
		echo "(>recipe)"${str:1}
	else
		export RIBS_RECIPE_ID=$((RIBS_RECIPE_ID+1))
		RECIPE_IDS[$RIBS_RECIPE_ID]="$str"
		export RECIPE_IDS
		clr_green "$RIBS_RECIPE_ID $(clr_cyan "$1 $2")"
	fi
}


function ribs_recipe_icons {
	icon=$1
	name=$2
	shift 2
	str="<img title='$name' src='/static/images/$icon.png' />"
	for i in "$@"; do
		str="$str|$i"
	done
	echo "(>recipe)"${str}
}


function ribs_prepare {
	if [[ "$1" == "Run" ]]
	then
		return 1
	else
		return 0
	fi
}

function end_prepare {
	exit
}

function start_block {
	echo "(>start_block)"
}

function end_block {
	echo "(>end_block)"
}

function ok() {
	clr_green "finished!"
}

function check_success() {
	if [ $? -eq 0 ]
	then
		clr_brown "=== done ======================================================================================="
		if [ ! -z "$1" ]
		then
			p_info "End session $1..."
			schroot --end-session -c "$1"
		fi
	else
		clr_red "=== Command Failed! ============================================================================"
		if [ ! -z "$1" ]
		then
			p_info "End session $1..."
			schroot --end-session -c "$1"
		fi
		echo "(>fail)"
		exit
	fi
}

function print_percent() {
	CURRENT=$1
	TOTAL=$2
	PERCENT=$((100*$CURRENT/$TOTAL))
	PERCENT=$(printf "%03d" $PERCENT)
	echo "[$PERCENT%]"
}

function checkPackage() {
	cd "$SHARD_PATH/data"
	clr_green "Downloading $1..."
	wget -c --quiet "http://data.ryzom.com/shard_datas/$1.tgz" || exit
	clr_green "Checking package integrity..."
	# check accidental corruption
	if [ ! -f "$SHARD_PATH/data/SHA256" ]
	then
		cp "$RIBS_BASE_PATH/data/SHA256" "$SHARD_PATH/data/"
	fi
	sha256sum -c --ignore-missing SHA256 2>/dev/null
	if [ $? -eq 0 ]
	then
		clr_green "Unpacking $1..."
		tar xzf "$1.tgz"
		rm -f "$1.tgz"
	else
		clr_red "Corrupted!"
		exit
	fi
}

function escape() {
	echo "$1" | sed -e 's/"/\\"/g'
}

function sanitize() {
	echo -E "$1" | sed "s/'//g;s/\"//g;s/\\\//g"
}

function repeat() {
	for i in $(seq 0 $2)
	do
		echo -n "$1"
	done
}

function p_title() {
	if [ "$RIBS_CONTEXT" == "WEB" ]
	then
		echo -n "<div style='color: orange; padding: 5px; font-size: 13pt; margin: auto'>🔵 $1</div>"
		if [ ! -z "$2" ]
		then
			echo -n "<img style='height: 50px' src='$2' />"
		fi
		echo -n "<div style='margin-bottom: 5px; height: 5px; background-color:#08c'></div>"
	else
		len=${#1}
		fill=$(( (70-$len)/2 ))
		clr_green "======================================================================"
		clr_green $(repeat " " $fill)"$1"$(repeat " " $fill)
		clr_green "======================================================================"
	fi

}
function p_hr() {
	if [ "$RIBS_CONTEXT" == "WEB" ]
	then
		echo -n  "<hr />"
	else
		repeat "-" 70
	fi
}

function p_action() {
	if [ "$RIBS_CONTEXT" == "WEB" ]
	then
		echo "<span style='display:inline-block; margin-left: 4px; color: lightgreen; font-size: 11pt;'>$1</span>"
	else
		clr_green "\t$1"
	fi
}

function p_info() {

		if [ "$RIBS_CONTEXT" == "WEB" ]
	then
		clr_layer "#CCC" "️ℹ️ $1"
	else
		clr_white "ℹ️ $1"
	fi
}

function p_ok() {
	clr_green "✅ $1"

}

function p_error() {
	clr_red "📛 $1"
}

function p_warning() {
	clr_brown "⚠️ $1"
}

