#!/bin/bash

SCRIPT=$1
PID=$2

/bin/bash $SCRIPT stop

screen -S RIBS_$PID -X quit

PIDLOG=$(ps aux | grep "tail -f /tmp/screenlog.$PID" | grep -v grep | awk '{print $2}')
PIDERR=$(ps aux | grep "tail -f /tmp/screenerr.$PID" | grep -v grep | awk '{print $2}')

kill -9 $PIDLOG
kill -9 $PIDERR
