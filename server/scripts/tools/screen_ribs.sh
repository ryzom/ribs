#!/bin/bash

BASEDIR="$(realpath $(dirname "$(readlink -f "$0")"))"
source $BASEDIR/web_colors.sh

RIBSNAME=$(echo $1 | sed 's#/#__#g')
screen -list | grep \\\.RIBS_$RIBSNAME
RET=$(screen -list | grep \\\.RIBS_$RIBSNAME | wc -l)
if [[ "$RET" == "1" ]]
then
	if [[ "$1" != "000_init.sh" ]]
	then
		clr_red "RIBS [$1] ALLREADY RUNNING! COOKING ABORTED"
		exit
	fi
fi

PID=$1
shift

rm /tmp/screenlog.$PID 2> /dev/null
rm /tmp/screenerr.$PID 2> /dev/null
rm /tmp/runscript.$PID 2> /dev/null

touch /tmp/screenlog.$PID
tail -f /tmp/screenlog.$PID &
TAIL_PID=$!

touch /tmp/screenerr.$PID
tail -f /tmp/screenerr.$PID 1>&2 &
TAILERR_PID=$!


#exec > >(tee -a /tmp/screenlog.$PID)

cat << EOF > /tmp/runscript.$PID
tail -f /tmp/screenlog.$PID &
tail -f /tmp/screenerr.$PID 1>&2 &
/bin/bash $* > /tmp/screenlog.$PID 2> /tmp/screenerr.$PID
echo "END"
sleep 1
kill $TAIL_PID
kill $TAILERR_PID
#rm /tmp/screenlog.$PID
#rm /tmp/screenerr.$PID
#rm /tmp/runscript.$PID
EOF

script=$(basename $1)
shift
#echo ">>> ribs $script $*"
chmod a+x /tmp/runscript.$PID
screen -dmS RIBS_$PID -c $BASEDIR/../data/screen.rc /tmp/runscript.$PID
