#!/bin/bash
#############################################
# | ___ \|_   _|| ___ \/  ___|
# | |_/ /  | |  | |_/ /\ `--.
# |    /   | |  | ___ \ `--. \
# | |\ \ __| |__| |_/ //\__/ /
# \_| \_(_)___(_)____(_)____(_)
#
# Remote Interface to Bash Scripts
# - better with barcebue sauce -
# Copyright (C) 2019 Nuneo (ulukyn@gmail.com)
#
# This program is free software (GPLv3): read https://www.gnu.org/licenses/gpl-3.0.en.html for more details
#
# Init Script
#

source "$RIBS_SCRIPT_PATH/tools/utils.sh"

# $RIBS_USER is defined only when user as been authenticated
if [ -z "$RIBS_USER" ]
then
	cd $RIBS_USER_PATH/recipes
	bash auth.sh $*
else
	. $RIBS_SCRIPT_PATH/data/welcome.txt
	display_user
	logged "$RIBS_USER"

	clr_magenta
	# Load recipes from user folder
	cd $RIBS_USER_PATH/recipes
	export RIBS_RECIPE_ID=0
	for main_recipes in *.sh
	do
		if [ "$main_recipes" != "auth.sh" ]
		then
			clr_magenta "Loading recipes $main_recipes"
			bash $main_recipes
		fi
	done
fi
