#!/bin/bash
#############################################
# | ___ \|_   _|| ___ \/  ___|
# | |_/ /  | |  | |_/ /\ `--.
# |    /   | |  | ___ \ `--. \
# | |\ \ __| |__| |_/ //\__/ /
# \_| \_(_)___(_)____(_)____(_)
#
# Remote Interface to Bash Scripts
# - better with barcebue sauce -
# Copyright (C) 2019 Nuneo (ulukyn@gmail.com)
#
# This program is free software (GPLv3): read https://www.gnu.org/licenses/gpl-3.0.en.html for more details
#
# Ribs install script
#

. scripts/tools/colors.sh

# http://0pointer.de/blog/projects/os-release
if [ -f /etc/os-release ]
then
	source /etc/os-release
else
	ID=ubuntu
fi

clr_green -n "OS Release: "
clr_brown "$ID"
pkgtool='sudo apt install'
# ubuntu debian fedora arch
if [ $ID == arch ] || [ $ID_LIKE == arch ]
then
	pkgtool='sudo pacman -S'
elif [ $ID == fedora ]
then
	pkgtool='sudo dnf install'
fi

clr_green -n "Package tool: "
clr_brown "$pkgtool"

# python-
ver=3
if [ $ID == arch ] || [ $ID_LIKE == arch ]
then
	ver=''
fi
# install packages
clr_green -n "Search for 'screen' tool: "
which screen
if [ $? -eq 0 ]
then
	clr_brown "Found!"
else
	clr_cyan "Missing. Installing..."
	$pkgtool screen || exit
fi

clr_green -n "Search for 'python$ver': "
which python$ver
if [ $? -eq 0 ]
then
	clr_brown "Found!"
else
	clr_cyan "Missing. Installing..."
	$pkgtool python$ver || exit
fi

clr_green -n "Search for 'pip$ver': "
which pip$ver
if [ $? -eq 0 ]
then
	clr_brown "Found!"
else
	clr_cyan "Missing. Installing..."
	$pkgtool python$ver-pip || exit
fi

clr_green -n "Search for 'realpath': "
which realpath
if [ $? -eq 0 ]
then
	clr_brown "Found!"
else
	clr_cyan "Missing. Installing..."
	$pkgtool realpath || exit
fi


# install packages

# setup virtual env
clr_green -n "Installing python virtualenv with pip: "
python3 -m pip install --user virtualenv || exit
clr_brown "Done!"
clr_green -n "Setuping python env: "
python3 -m virtualenv env || exit
clr_brown "Done!"

clr_green -n "Activating python env: "
source env/bin/activate
clr_brown "Done!"

clr_green -n "Installing tools on python env: "
# install python libs
pip3 install gevent colorama simple-crypt flask psutil simplediff
deactivate
clr_brown "Done!"

if [ ! -f config.sh ]
then
	cp config_default.sh config.sh || exit
	clr_red "Please change config.sh before start the RIBS server"
else
	clr_green "All done! Ribs instaled and ready to launch"
fi

echo "Please add RIBS_BASE_PATH to you profile"
