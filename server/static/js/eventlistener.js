/*#############################################
# | ___ \|_   _|| ___ \/  ___|
# | |_/ /  | |  | |_/ /\ `--.
# |    /   | |  | ___ \ `--. \
# | |\ \ __| |__| |_/ //\__/ /
# \_| \_(_)___(_)____(_)____(_)
#
# Remote Interface to Bash Scripts
# - better with barcebue sauce -
# Copyright (C) 2019 Nuneo (ulukyn@gmail.com)
#
# This program is free software (GPLv3): read https://www.gnu.org/licenses/gpl-3.0.en.html for more details
#
# Eventlistener class
#
*/


class EventListener {

	event_refresh(e) {
		console.log("<<< refresh");
		var data = JSON.parse(e.data);
		if (data[1] != app.uuid)
			return;
		location.reload();
	}

	event_reprepare(e) {
		console.log("<<< reprepare");
		console.log(e.data);
		if (e.data != app.uuid)
			return;
		console.log("re-prepare now!");
		app.prepareRecipe();
	}

	event_recipe(e) {
		var data = JSON.parse(e.data)
		if (e.data) {
			console.log("<<< recipe : "+data);
			let option_div = $("#recipes-content");
			let params = data[1].split("|");
			console.log(params)

			let script = params.shift();
			let icon = params.shift();
			let recipe_name = params.shift();
			console.log("<<< recipe : "+recipe_name);
			app.recipes[recipe_name] = new Recipe(recipe_name);

			for (let rib in params) {

				let ribs = new Ribs();

				let options = params[rib].split(",");
				ribs.script_file = options.shift().trim();

				let script_name = options.shift();

				// Enable it by default, only disable if prefixed by -
				if (script_name[0] == "-") {
					ribs.enable = false;
					ribs.name = script_name.substr(1);
				} else {
					ribs.name = script_name;
				}

				ribs.options = options;

				app.recipes[recipe_name].addRibs(ribs);

				/*if (script_name) {
					var scriptID = script_file.replace(".sh", "");
					console.log("add input count for "+scriptID);
					app.inputcount[scriptID] = [0];
				}*/

				// script_name, all_options, is_active?, selected_options, script_file
				//app.recipes[recipe_name].push(
				//	[script_name, scripts, enable, {}, {}, script_file]);
			}

			// Recipes starting with "-" are hidden (auth use them for example). Auto select them
			if (recipe_name[0] == "-")
				app.selectRecipe(false, recipe_name, false);
			else
				option_div.append('<div class="recipe" onclick="app.selectRecipe(this, \''+recipe_name+'\')"><span style="font-size: 14pt">'+icon+'</span> '+recipe_name+'</div>');
		}
	}

	event_flavour(e) {
		console.warn("<<< flavour");
		console.log(e.data);
		if (e.data) {
			console.log("get flavour: ", e.data);
			var option_div = $("#flavours-content");
			var flavours = e.data.split("|");
			var ribs = flavours.shift();
			var current_flavour = flavours.shift();
			var bg = flavours.shift();
			$("#current-flavour").html(current_flavour);
			$("#header").css("background-image", "url("+bg+".jpg)");
			$("#right").css("background-image", "url("+bg+"_right.jpg)");
			$("#outputs").css("background-image", "url("+bg+"_left.jpg)");
			$(".output").css("background-color", "rgba(15, 15, 15, 0.98)");

			for (var flavour in flavours)
				option_div.append('<div class="flavour" onclick="app.selectFlavour(\''+flavours[flavour]+'\')">'+flavours[flavour]+'</div>');
		}
	}

	event_list(e) {
		console.log("<<< list");
		var data = JSON.parse(e.data)
		console.log(data);
		var cooker = data[0]
		var dialog = data[1].split("|");
		var script = dialog.shift();
		var scriptID = script.replace(".sh", "");
		if (app.inputcount[scriptID] == undefined)
			app.inputcount[scriptID] = [0];
		app.inputcount[scriptID][0]++;
		let id = app.inputcount[scriptID][0];

		var html = "<div class='dialog'>";
		for (var d in dialog) {
			var sd = dialog[d].split(":");
			var sd_key = sd.shift();
			if (sd_key)
				html += '<label class="radiobox">'+sd.join(':')+'<input type="radio" name="'+scriptID+'_'+id+'" onchange="app.setInputValue(\''+scriptID+'\', '+id+', \''+sd_key+'\', \'1\', 0)" /><span class="checkmark"></span></label>';
		}
		html += "</div>";
		app.appendOutput("kitchen", html, false);
		console.log("set value input count for "+scriptID, app.inputcount);
		app.inputcount[scriptID][id] = 0;
	}

	event_inputText(e) {
		console.log("<<< inputText");
		var data = JSON.parse(e.data)
		if (app.currentFollowId != "kitchen")
			return;
		console.log("INPUT: "+data[1])
		var dialog = data[1].split("|");
		var script = dialog.shift();
		var scriptID = script.replace(".sh", "");
		var sd_key = dialog.shift();
		if (app.inputcount[scriptID] == undefined)
			app.inputcount[scriptID] = [0];
		app.inputcount[scriptID][0]++;
		var id = app.inputcount[scriptID][0];
		var value = dialog.shift();
		var html = '<div class="dialog"><input name="'+sd_key+'" type="text" class="input" value="'+escapeHtml(value)+'" oninput="app.setInputValue(\''+scriptID+'\', '+id+', \''+sd_key+'\', $(this).val(), 0)" /></div>';
		app.appendOutput(data[0], html, false);
		app.inputcount[scriptID][id] = 0;
		if (value)
			app.setInputValue(scriptID, id, sd_key, value, false);
	}

	event_inputPassword(e) {
		console.log("<<< inputPassword");
		var data = JSON.parse(e.data)
		if (app.currentFollowId != "kitchen")
			return;
		console.log("INPUT: "+data[1]);
		var dialog = data[1].split("|");
		var script = dialog.shift();
		var scriptID = script.replace(".sh", "");
		var sd_key = dialog.shift();
		if (app.inputcount[scriptID] == undefined)
			app.inputcount[scriptID] = [0];
		app.inputcount[scriptID][0]++;
		var id = app.inputcount[scriptID][0];
		var html = '<div class="dialog"><input name="'+sd_key+'" type="password" class="input" oninput="app.setInputValue(\''+scriptID+'\', '+id+', \''+sd_key+'\', $(this).val(), 1)" /></div>'
		app.appendOutput(data[0],html, false);
		app.inputcount[scriptID][id] = 0;
	}

	event_ask(e) {
		console.log("<<< ask");
		var data = JSON.parse(e.data)
		console.log(data);
		var cooker = data[0]
		var dialog = data[1].split("|")
		var ribs = dialog.shift().replace(".sh", "")
		var message = dialog.shift()
		if (ribs == "ssskitchen")
			ribs = "";
		app.appendOutput(data[0], "<br /><div class='ask'><span class='ask_for_script'>"+ribs+"</span> "+message+"</div>", false);
	}

	event_logged(e) {
		console.log("<<< logged");
		var data = JSON.parse(e.data)
		console.log(data);
		$("#user").html(data[1])
	}

	event_clear(e) {
		console.log("<<< clear");
		console.log(e.data);
		//app.inputcount = {}
		app.clearOutput()
	}

	event_progress(e) {
		console.log("<<< progress");
		var data = JSON.parse(e.data);
		var cooker = data[0]
		var dialog = data[1].split("|")
		var percent = dialog.shift()
		var text = dialog.shift()
		$("#cooker-activity").css("background-size", percent+"%");
		app.appendOutput(cooker, text)
	}


	event_prepare(e) {
		console.log("<<< prepare");
		var data = JSON.parse(e.data)
		console.log(data)
		var cooking = data[0]
		var data = data[1]
		var ribs = data[0]
		var options = data[1]
		app.activities[cooking] = "Preparing <span class='activity'>"+ribs+" "+options+"</span>"
		app.displayCookActivity()
		app.appendOutput(cooking, "<div class='preparing'>Preparing "+ribs+" "+options+"</div>", false);
	}

	event_running(e) {
		console.log("<<< running");
		var data = JSON.parse(e.data)
		var cooking = data[0]
		var ribs = data[1][0]
		var params = data[1][1]

		app.activities[cooking] = "Cooking <span class='activity'>"+ribs+" "+params+"</span>"
		app.displayCookActivity()
		//app.appendOutput(cooking, "<div class='running'><strong>Cooking</strong> "+ribs+" "+params+"</div>", false);
	}


	event_prepared(e) {
		console.log("<<< prepared");
		var data = JSON.parse(e.data)
		console.log(data);
		var cooking = data[0]
		var ribs = data[1]
		if (ribs[0] == "000_init.sh")
			return;
		app.cooked_ribs++;
		if (app.cooked_ribs >= app.cooking_ribs.length) {
			app.cooked_ribs = 0;
			app.cooking_ribs = [];
			app.activities[cooking] = "Waiting answer...";

			if (app.checkInputFilled()) {
				app.activities[cooking] = "Finished"
				app.displayActionButton("cook")
				app.appendOutput("kitchen", "<div class=\"blue-button\" onclick=\"app.cookRecipe()\">All ready. Start cooking!</div>", false);
			} else {
				app.displayActionButton("answer")
				app.appendOutput("kitchen", "<div class='finished'>ALL PREPARED!</div>", false);
			}
			app.displayCookActivity();
		} else
			app.appendOutput("kitchen", "<div class='prepared'>Prepared!</div>", false);
	}

	event_finished(e) {
		console.log("<<< finished");
		var data = JSON.parse(e.data)
		var cooking = data[0]
		var ribs = data[1][0]
		var orderer = data[1][1]

		app.activities[cooking] = "Finished"
		app.displayCookActivity();

		console.log(orderer, app.uuid, data)
		//if (orderer != app.uuid)
		//	return;

		app.cooked_ribs++;
		app.displayActionButton("")
		if (ribs != "000_init.sh")
			app.appendOutput(cooking, "<div class='finished'>Finished</div>")
		$("#cooker-activity").css("background-size", "0%")
	}

	event_stopped(e) {
		console.log("<<< stopped");
		console.log(e.data);
		var dialog = e.data.replace("<br />", "").split("|")
		var script = dialog.shift()
		var cooker_id = dialog.shift()
		app.activities[cooker_id] = "Stopped"
		app.displayCookActivity();

		if (cooker_id != app.currentFollowId)
			return;

		app.cooking_ribs = [];
		app.cooked_ribs = 0;
		app.displayActionButton("")
		if (script != "000_init.sh")
			app.appendOutput("<div class='finished'>STOPPED</div>")
		$("#cooker-activity").css("background-size", "0%")
	}

	event_ping(e) {
		var data = JSON.parse(e.data)
		data = data[1]
		app.time = data[0];
		var cookings = data[1];
		if (cookings != undefined) {
			let count = Object.keys(cookings).length;
			console.log("Ping and "+count+" Cooking ribs");

			if (count > 0) {
				let need_updated = false;
				for (let cooking in cookings) {
					if (cooking != "kitchen") {
						if (app.cooks[cleanName(cooking)] == undefined) {
							app.cooks[cleanName(cooking)] = cooking;
							need_updated = true;
						}
					}
				}
				if (need_updated)
					app.displayTabs();
			}
		}
	}

	event_setCookers(e) {
		console.log("<<< setCookers");
		console.log(e.data);
		app.cooks = {};
		var data = e.data.replace("<br />", "").split("|");
		for (var cooker in data) {
			var d = data[cooker].split(":");
			app.cooks[d[1]] = d[0];
			console.log("New Cooker: "+d[0]+" = "+d[1]);
		}
		app.displayTabs();
	}

	event_newCook(e) {
		console.log("<<< newCooker");
		var data = JSON.parse(e.data);
		if (data[1] != "kitchen") {
			app.cooks[cleanName(data[1])] = data[1];
			app.displayTabs();
		}
	}

	event_delCook(e) {
		console.log("<<< delCooker");
		var data = JSON.parse(e.data);
		if (data[1] != "kitchen") {
			app.cooks[cleanName(data[1])] = null;
			app.displayTabs();
		}
	}

	event_output(e) {
		if (e.data) {
			let data = JSON.parse(e.data);
			app.appendOutput(cleanName(data[0]), data[1])
			$("#status").html(_i("32/bullet_green"))
		}
	}

	event_text(e) {
		if (e.data) {
			let data = JSON.parse(e.data);
			//console.log(e.data)
			app.appendOutput(cleanName(data[0]), data[1], false)
		}
	}

	event_logs(e) {
		console.log("<<< logs");
		var data = JSON.parse(e.data);
		app.appendLog(data[1]+"<br />")
	}

	event_activity(e) {
		console.log("<<< activity");
		if (e.data) {
			let data = JSON.parse(e.data);
			app.activities[cleanName(data[0])] = data[1]
			app.displayCookActivity();
		}
	}

	event_sysInfo(e) {
		console.log("<<< sysInfo");
		console.log(e.data);
		$("#sysinfo").html(e.data.replace("<br />", ""))
	}

	event_error(e) {
		console.log("<<< error");
		if (e.data) {
			var data = JSON.parse(e.data);
			console.error(data[1]);
			app.appendOutput(data[0], "<span class='error'>"+data[1]+"</span>")
			$("#status").html(_i("32/bullet_blue"))
		}
	}

}

