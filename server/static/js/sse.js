/*#############################################
# | ___ \|_   _|| ___ \/  ___|
# | |_/ /  | |  | |_/ /\ `--.
# |    /   | |  | ___ \ `--. \
# | |\ \ __| |__| |_/ //\__/ /
# \_| \_(_)___(_)____(_)____(_)
#
# Remote Interface to Bash Scripts
# - better with barcebue sauce -
# Copyright (C) 2019 Nuneo (ulukyn@gmail.com)
#
# This program is free software (GPLv3): read https://www.gnu.org/licenses/gpl-3.0.en.html for more details
#
# Server-sent Events management
#
*/

function _i(name) {
	return '<img src="/static/images/icons/'+name+'.png" style="vertical-align: middle;"/>'
}

function cleanName(name) {
	return name.replace(" ", "_")
}

function escapeHtml(text) {
  var map = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#039;'
  };

  return text.replace(/[&<>"']/g, function(m) { return map[m]; });
}

class App {

	start() {
		// Create User ID (change at each refresh)
		this.uuid = this.guid();

		// Create EventSource
		this.source = new ReconnectingEventSource("/stream/"+this.uuid);
		var eL = new EventListener();
		this.currentFollowId = "kitchen";
		this.cooks = {}
		this.cooking_ribs = [];
		this.cooked_ribs = 0;
		this.need_form = false
		this.inputcount = {}
		this.outputs = {}
		this.recipes = {}
		this.active_recipe = "";
		this.new_cooking = "";
		this.activities = {}
		this.clients = {}
		this.clients[this.uuid] = "Me"
		this.status = "init"
		this.lastScrollTop = 0

		console.log("--- Start APP ---")

		this.source.onerror = function(e) {
			// Error
			console.error("error", e)
			$("#status").html(_i("32/bullet_red"))
		}

		this.source.onopen = function(e) {
			// Open
			console.log("EventSource Opened!")
			$("#status").html(_i("32/bullet_green"))
			// prevent duplicate
			$("#recipes-content").empty()
		}

		// Add all EventListener "event_" methods to event source
		var pros = [];
		var obj = eL;
		do {
			pros = pros.concat(Object.getOwnPropertyNames(obj));
		} while (obj = Object.getPrototypeOf(obj));

		var eventListenerFunctions = pros.sort().filter(
			function(e, i, arr) {
				if (e.substr(0, 6) == "event_")
					return true;
			}
		);

		for(var i in eventListenerFunctions) {
			var func = eventListenerFunctions[i];
			this.source.addEventListener(
				eventListenerFunctions[i].substr(6),
				eL[eventListenerFunctions[i]]
			);
		}
	}

	guid() {
		function s4() {
			return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
		}
		return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
	}

	createOutput(tab_id) {
		console.warn(tab_id)
		var e = $("#output-"+tab_id);
		console.log(e)
		if (!e.length) {
			if (tab_id != "kitchen")
				$("#outputs").append("<div id='output-"+tab_id+"' class='output' style='display: none'><div id='stop-"+tab_id+"' onclick='app.stopCook(\""+tab_id+"\")' class='stop-cooker'>STOP</div></div>");
			else
				$("#outputs").append("<div id='output-"+tab_id+"' class='output'></div>");
		}
	}

/// Manage Output
		appendOutput(tab_id, html, use_timer=false, new_line=true) {
		tab_id = cleanName(tab_id)
		if (tab_id == "")
			tab_id = "kitchen"

		this.createOutput(tab_id);
		if (cleanName(this.new_cooking) == tab_id) {
			this.new_cooking = "";
			this.follow(tab_id, true)
		}

		var e = $("#output-"+tab_id);
		var need_auto_scroll = e.prop("scrollHeight") - e.prop("scrollTop") - e.prop("clientHeight") < 30;
		//var need_auto_scroll = this.lastScrollTop == e.prop("scrollTop")

		if (html) {
			if (use_timer)
				e.append("<span class='time'>"+this.time+"</span> "+html.replace(/\\n/g, "\n")+"<br />");
			else
				if (new_line)
					e.append(html+"<br />");
				else
					e.append(html.replace(/\\n/g, "\n"));
		}
		if (need_auto_scroll) {
			e.scrollTop(e.prop("scrollHeight"));
			this.lastScrollTop = e.prop("scrollTop");
		}
	}

	clearOutput(tab_id) {
		$("#output-"+tab_id).empty();
		if (tab_id in this.cooks) {
			delete this.cooks[tab_id];
			this.follow("kitchen");
		}
		this.displayTabs();
	}

	appendLog(html) {
		var e = $("#output-logs")
		var need_auto_scroll = e.prop("scrollHeight") - e.prop("scrollTop") == e.prop("clientHeight")
		if (html)
			e.append(html)
		if (need_auto_scroll)
			e.scrollTop(e.prop("scrollHeight"));
	}

	displayCookActivity() {
		console.warn("displayCookActivity");
		var tab_id = this.currentFollowId;
		console.log(tab_id)
		console.log(this.activities)
		var activity = this.activities[tab_id]
		if (activity == undefined)
			$("#cooker-activity").html("Idle...");
		else
			$("#cooker-activity").html(activity);
	}

	displayActionButton(name) {
		if (name != "answer")
			$("#answer").css("display", "none")
		if (name != "cook")
			$("#cook").css("display", "none")
		if (name != "prepare")
			$("#prepare").css("display", "none")
		if (name)
			$("#"+name).css("display", "block")
	}

	displayTabs() {
		var html = "<span id='logs' onclick='app.follow(\"logs\")'><img src=\"static/images/logs.png\"/> Logs</span>";
		var selected = "";
		if (this.currentFollowId == "kitchen")
			selected = "class='selected'";

		html += "<span id='kitchen' "+selected+" onclick='app.follow(\"kitchen\")'><img src=\"static/images/kitchen.png\"/> Kitchen</span>";
		console.log(this.cooks)
		for (let cook in this.cooks) {
			if (cook.match(/[a-f0-9]{8}\-[a-f0-9]{4}\-[a-f0-9]{4}\-[a-f0-9]{4}\-[a-f0-9]{12}/) != null || cook[0] == "-")
				continue;
			console.log("Display Tags (current = "+this.currentFollowId +": "+cook)
			selected = "";
			if (this.currentFollowId == cook)
				selected = "selected";

			if (this.cooks[cook] == null) {
				html += "<span id='"+cook+"' class='closable-left "+selected+"' onclick='app.follow(\""+cook+"\")'><img src=\"static/images/ready_ribs.png\" /> "+cook.replace("_", " ")+"</span><span id='clear-"+cook+"' class='closable-right "+selected+"' onclick='app.clearOutput(\""+cook+"\")'>"+_i("32/cross")+"</span>";
				$("stop-"+cook).css("display", "none");
			} else {
				html += "<span id='"+cook+"' class='"+selected+"' onclick='app.follow(\""+cook+"\")'><img src=\"static/images/cooker.png\"/> "+cook.replace("_", " ")+"</span>";
				$("stop-"+cook).css("display", "block");
			}
		}
		$("#cookers").html(html);
		this.displayCookActivity();
	}


/// Recipe functions
	resetRecipePreparation(remove_run) {
		// Clean selected options in preparation of recipe to ask again in next prepare
		/*for(var script in this.recipes[this.active_recipe]) {
			this.recipes[this.active_recipe][script][3] = this.recipes[this.active_recipe][script][3].slice(remove_run, this.recipes[this.active_recipe][script][1].length+remove_run)
		}*/
		this.inputcount = {}
	}

	cookRibs(recipe_name, ribs, is_run) {
		console.log(">>> Cook Ribs: "+recipe_name);
		console.log(ribs);
		this.new_cooking = recipe_name;
		this.cooking_ribs = ribs;
		this.cooked_ribs = 0;
		var data = window.btoa(JSON.stringify(ribs))+"/"+is_run;

		console.log("/cook/"+this.uuid+"/"+recipe_name+"/"+data);

		$.ajax({
			url: "/cook/"+this.uuid+"/"+recipe_name+"/"+data,
		});

		if (is_run)
			this.appendOutput("kitchen", "<div class='running'>Running: "+ribs+"</div>", false);

		return false;
	}


	launchRecipe(command) {
		var scripts_to_run = [];
		var recipe_name = this.active_recipe;
		var ribs = this.recipes[recipe_name].getRibs();
		var is_run;
		for (let id in ribs) {
			let script = ribs[id];
			var options = [];
			if (script.enabled) {
				for (let opt in script.selected_options)
					if (script.selected_options[opt])
						options.push(opt);

				for (let opt in script.form_options)
					options.push([opt+"="+script.form_options[opt][0], script.form_options[opt][1]]);

				// Set - if no options (because script must have a least one arg)
				if (options.length == 0)
					options.push(["-", 0]);

				if (command) {
					is_run = "1";
					options.unshift([command, 0]);
				} else
					is_run = "0";

				scripts_to_run.push([script.script_file, options]);
			}
		}
		this.cookRibs(recipe_name, scripts_to_run, is_run);
	}


	prepareRecipe() {
		var guid = this.currentFollowId;
		this.activities[guid] = "Preparing Recipe..."
		this.status = "prepare";
		this.displayActionButton("");
		this.launchRecipe("");
		this.clearOutput("kitchen");
		//this.appendOutput("<br />");
	}

	cookRecipe() {
		console.log(this.recipes[this.active_recipe])
		this.status = "cook";
		this.launchRecipe("Run");
		//this.appendOutput("<br />");
		this.resetRecipePreparation(1);
	}

	stopCook(cooker) {
		console.log(">>> Stop Cooking");
		$.ajax({
			url: "/stop/"+this.uuid+"/"+cooker.replace("_", " "),
		})
	}


	openUrl(url) {
		console.log(">>> Open Url : "+url);
		$.ajax({
			url: "/"+url+"/"+this.uuid,
		})

		return false;
	}

	selectRecipe(element, recipe_name, show_form=true) {
		console.log("# SelectRecipe : "+recipe_name);
		$(".recipe").each(function(e) {
			$(this).removeClass("recipe-selected");
		});

		$(element).addClass("recipe-selected");
		this.active_recipe = recipe_name;
		this.resetRecipePreparation(0);
		if (show_form)
			this.displayActionButton("prepare");
		let ribs = this.recipes[recipe_name].getRibs();
		if (ribs.length > 0) {
			let html = ""
			for (let id in ribs) {
				let script = ribs[id];
				//this.recipes[recipe_name][script][4] = {}
				let script_name = script.name;
				let script_options = script.options;
				let script_selected_options = script.selected_options;
				let script_file = script.script_file;
				let selected;
				if (script.enabled)
					selected = "selected-script";
				else
					selected = "unselected-script";

				html += '<div class="script"><span id="'+cleanName(recipe_name+'_'+id)+'" title="'+script_file+'" onclick="app.selectScript(\''+recipe_name+'\', \''+id+'\')" class="'+selected+'">'+script_name+'</span><div class="script-option-content">'
				for (var option in script_options) {
					var option_name = script_options[option]
					selected = "";
					if (option_name[0] == "+") {
						option_name = option_name.substr(1);
						if (script_selected_options[option_name] == undefined || script_selected_options[option_name])
							selected = "selected";
					} else if (script_selected_options[option_name] != undefined && script_selected_options[option_name])
							selected = "selected";

					script.selected_options[option_name] = selected == "selected";

					html += '<div id="'+cleanName(recipe_name+'_'+id+'_'+option_name)+'" class="'+selected+' script-option" onclick="app.selectScriptOption(\''+recipe_name+'\', \''+id+'\', \''+option_name+'\')">'+option_name+'</div>'
				}
				html += '</div></div>';
			}

			if (show_form) {
				var option_div = $("#scripts-content")
				option_div.html(html)
			}
		}
	}


	selectScript(recipe_name, script_id) {
		console.log(recipe_name, script_id, this.recipes);
		let script = this.recipes[recipe_name].getRibById(script_id);
		console.log(script);
		if (script != undefined) {
			var e = $("#"+cleanName(recipe_name+'_'+script_id));
			if (script.enabled) {
				script.enabled = false;
				e.removeClass("selected-script");
				e.addClass("unselected-script");
			} else {
				script.enabled = true;
				e.removeClass("unselected-script");
				e.addClass("selected-script");
			}
		}
	}

	selectScriptOption(recipe_name, script_id, option_name) {
		let script = this.recipes[recipe_name].getRibById(script_id);
		let options = script.selected_options;
		var e = $("#"+cleanName(recipe_name+'_'+script_id+'_'+option_name));
		//console.log(recipe_name, script, option, option_name, index)

		if (e.hasClass("selected")) {
			script.selected_options[option_name] = false;
			e.removeClass("selected");
		} else {
			script.selected_options[option_name] = true;
			e.addClass("selected");
		}
		console.log(script.selected_options);
	}

	checkInputFilled() {
		var recipe_name = this.active_recipe;
		var all_filled = true;
		if (this.recipes[recipe_name] != undefined) {
			let ribs = this.recipes[recipe_name].getRibs();
			for (let id in ribs) {
				let script = ribs[id];
				if (script.enabled) {
					var script_name = script.script_file.replace(".sh", "");
					for (var input in this.inputcount[script_name]) {
						if (this.inputcount[script_name][input] == 0) {
							all_filled = false;
						}
					}
				}
			}
			return all_filled;
		}
	}

	setInputValue(script_name, input_id, name, value, is_password) {
		var recipe_name = this.active_recipe;
		let ribs = this.recipes[recipe_name].getRibs();
		console.log(ribs);
		for (let id in ribs) {
			let script = ribs[id];
			console.log(script_name+".sh", script.script_file);
			if (script_name+".sh" == script.script_file) {
				script.form_options[name] = [value, is_password];
				console.log("inputcount", script_name, input_id)
				this.inputcount[script_name][input_id] = 1;
				//console.log(this.recipes[recipe_name][script_id][3])
			}
		}

		if (this.checkInputFilled())
			this.displayActionButton("cook")
	}


	follow(cook, no_ajax) {
		console.log("follow : "+cook);
		var tab_id = cleanName(cook);
		if (cook != "logs")
			$("#logs").removeClass("selected");

		$("#"+this.currentFollowId).removeClass("selected");
		$("#clear-"+this.currentFollowId).removeClass("selected");
		$(".output").each(function(i) {
			$(this).css("display", "none");
		})

		if (cook != "logs")
			this.currentFollow = $("#"+cook);

		$("#"+tab_id).addClass("selected");
		$("#clear-"+tab_id).addClass("selected");
		this.createOutput(tab_id);
		$("#output-"+tab_id).css("display", "block");

		if (cook != "kitchen") {
			$("#right").css("display", "none");
			var script_on_client = this.clients[cook];
			this.appendOutput("");
		} else {
			$("#right").css("display", "flex");
		}

		if (cook != "logs") {
			this.currentFollowId = tab_id;
			this.displayCookActivity();
		} else
			$("#cooker-activity").html("Logs");

		if (no_ajax != true)
			$.ajax({url: "/follow/"+this.uuid+"/"+cook});

		if (cook == "logs") {
			$("#output-logs").empty();
			//this.appendOutput("");
		}
	}
}

app = new App();

$(document).ready(function() {
	app.start();
	app.displayTabs();
	app.follow("kitchen");
});
