/*#############################################
# | ___ \|_   _|| ___ \/  ___|
# | |_/ /  | |  | |_/ /\ `--.
# |    /   | |  | ___ \ `--. \
# | |\ \ __| |__| |_/ //\__/ /
# \_| \_(_)___(_)____(_)____(_)
#
# Remote Interface to Bash Scripts
# - better with barcebue sauce -
# Copyright (C) 2019 Nuneo (ulukyn@gmail.com)
#
# This program is free software (GPLv3): read https://www.gnu.org/licenses/gpl-3.0.en.html for more details
#
# Recipe class
#
*/


class Recipe {

	constructor(name) {
		this.name = name;
		this.enabled = true;
		this.ribs = []
	}

	addRibs(ribs) {
		this.ribs.push(ribs)
	}

	getRibs() {
		return this.ribs;
	}

	getRibByName(name) {
		for (let id in this.ribs) {
			if (this.ribs[id].name == name)
				return this.ribs[id];
		}
		return undefined;
	}

	getRibById(id) {
		return this.ribs[id];
	}

}

