#!/bin/bash
#############################################
# | ___ \|_   _|| ___ \/  ___|
# | |_/ /  | |  | |_/ /\ `--.
# |    /   | |  | ___ \ `--. \
# | |\ \ __| |__| |_/ //\__/ /
# \_| \_(_)___(_)____(_)____(_)
#
# Remote Interface to Bash Scripts
# - better with barcebue sauce -
# Copyright (C) 2019 Nuneo (ulukyn@gmail.com)
#
# This program is free software (GPLv3): read https://www.gnu.org/licenses/gpl-3.0.en.html for more details
#
# Run script to use ribs without web interface
#

BASEDIR="$(realpath $(dirname "$(readlink -f "$0")")/..)"

. $BASEDIR/server/scripts/tools/colors.sh
echo basedir = $BASEDIR
RIBSNAME=$(echo $1 | sed 's#/#__#g')
RET=$(screen -list | grep \\\.RIBS_$RIBSNAME | wc -l)
if [[ "$RET" == "1" ]]
then
	clr_red "RIBS [$1] ALLREADY RUNNING! COOKING ABORTED"
	exit 1
fi



. $BASEDIR/server/config.sh

screen -S RIBS_$RIBSNAME -c $BASEDIR/server/scripts/data/screen.rc bash $RIBS_USER_SCRIPT_PATH/$1 
