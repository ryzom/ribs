#!/bin/bash

D=$1

if [ "$2" == "_wk" ]
then
    SUFFIX=$2
else
    SUFFIX=""
fi

echo -n "Generating Translations($SUFFIX) into $1..."

for lang in en fr de es ru
do
	wget -q https://me.ryzom.com/translations/${lang}${SUFFIX}.uxt -O $D/${lang}.uxt
	wget -q https://me.ryzom.com/translations/item_words_${lang}${SUFFIX}.txt -O $D/item_words_${lang}.txt
	wget -q https://me.ryzom.com/translations/creature_words_${lang}${SUFFIX}.txt -O $D/creature_words_${lang}.txt
	wget -q https://me.ryzom.com/translations/sbrick_words_${lang}${SUFFIX}.txt -O $D/sbrick_words_${lang}.txt
	wget -q https://me.ryzom.com/translations/place_words_${lang}${SUFFIX}.txt -O $D/place_words_${lang}.txt
	wget -q https://me.ryzom.com/translations/title_words_${lang}${SUFFIX}.txt -O $D/title_words_${lang}.txt
	wget -q https://app.ryzom.com/app_arcc/get_titles.php?lang=${lang} -O $D/title_ark_${lang}.txt
	cat $D/title_ark_${lang}.txt >> $D/title_words_${lang}.txt
	rm $D/title_ark_${lang}.txt
done
wget -q https://me.ryzom.com/translations/bot_names.txt -O $D/bot_names.txt

curl "https://api.ryzom.com/git_hook.php?project=ribs&token=3GCQqdEEZo6hWnQu2oKTXk6z4mPhKf2URk7KKCcXqp73QgpF"

echo "OK"