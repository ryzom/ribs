import re
import sys

if len(sys.argv) > 1:
    unpack_folder = sys.argv[1]
    translation_folder = sys.argv[2]
    print(f"Pars translation files from, {translation_folder}, to {unpack_folder}!")
else:
    print("ERROR: missing Paths!")
    sys.exit(1)

def parse_emote_file(filename):
    with open(filename, "r", encoding="utf-8") as file:
        content = file.read()
    
    emote_data = {}
    
    emote_blocks = re.findall(r"(EMOTE_\w+) \(.*?\)\s*{(.*?)}", content, re.DOTALL)
    
    for emote_name, block in emote_blocks:
        matches = re.findall(r"\((?:a\.gender = (\w+))\)\s+(\S+)\s+\[.*?&(.*)\]", block)
        
        if matches:
            for gender, emote_key, text in matches:
                formatted_text = text.replace("$a$", "[a]").replace("$t$", "[t]").replace("EMT&", "").replace("\"", "\\\"")
                lua_key = f"{emote_name}_{gender[0]}"
                emote_data[lua_key] = formatted_text.strip()
        else:
            default_match = re.search(r"(\S+)\s+\[.*?&(.*)\]", block)
            if default_match:
                formatted_text = default_match.group(2).replace("$a$", "[a]").replace("$t$", "[t]").replace("EMT&", "").replace("\"", "\\\"")
                emote_data[emote_name] = formatted_text.strip()
    
    return emote_data

def save_as_lua(data, filename, lang):
    with open(filename, "w", encoding="utf-8") as lua_file:
        lua_file.write("if (game==nil) then\n    game= {};\nend\n\n")
        lua_file.write(f"local langCode = getClientCfgVar(\"LanguageCode\")\n")
        lua_file.write(f"if (langCode == \"{lang}\") then\n")
        lua_file.write("    game.emots_text_array = {\n")
        for key, value in data.items():
            lua_file.write(f'        ["{key}"] = "{value}",\n')
        lua_file.write("    }\nend\n")

languages = ["en", "de", "fr", "es", "ru"]

for lang in languages:
    filename = f"{translation_folder}/phrase_{lang}.txt"
    emote_data = parse_emote_file(filename)
    
    lua_filename = f"{unpack_folder}/emotes_{lang}.lua"
    save_as_lua(emote_data, lua_filename, lang)
    
    print(f"Lua-File created: {lua_filename}")
