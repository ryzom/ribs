#!/bin/bash

echo -n "Generating Emote Texte for chat_command_autocomplete... "

UNPACKDIR=$1

if [ "$2" == "_wk" ]
then
    TRANSLATIONDIR=$3
else
    TRANSLATIONDIR=$2
fi


current_dir=$(dirname "${BASH_SOURCE[0]}")
python $current_dir/pars_emotes.py $UNPACKDIR $TRANSLATIONDIR

echo "OK"
