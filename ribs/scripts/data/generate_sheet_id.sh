#!/bin/bash

source "$RIBS_SCRIPT_PATH/tools/utils.sh"
source "$RIBS_SCRIPT_PATH/tools/rocket.sh"

source "$RIBS_BASE_PATH/data/ryzom_prefs.sh"

if [[ ! "$RIBS_GROUPS" == *":dev:"* ]]
then
	exit
fi

if [[ -z "$*" ]]
then
	ribs_script "data/generate_sheet_id.sh" "Generate sheet_id.bin"
	end_prepare
fi

if ribs_prepare $1
then
	cd $RYZOMSERVERDATA_PATH
	clr_brown "Changes from Atys (master branch):"
	#git diff master yubo --stat common/data_leveldesign/leveldesign
	end_prepare
fi

FILESHEET=sheet_id.bin

if [ -e $RYZOMSERVERDATA_PATH/game_element/$FILESHEET ]
then
	cp $RYZOMSERVERDATA_PATH/game_element/$FILESHEET ~/tmp/$FILESHEET.old.`date +%s`
fi

if [[ "$SHARD_TYPE" == "test" ]]
then
	send_chat DATA_BUILD "" "*sheet_id.bin*: Generating..." ":book:" "#FFC200"
fi

clr_green "Generating sheet_id.bin..."
make_sheet_id -o$RYZOMSERVERDATA_PATH/game_element/$FILESHEET \
	$RYZOMSERVERDATA_PATH/game_element \
	$RYZOMDATA_PATH/leveldesign/world \
	$RYZOMSERVERDATA_PATH/data/mirror_sheets

clr_green "Remove all packed sheets from server"
rm -f $SHARD_PATH/data/leveldesign/*.packed_sheets

if [[ "$SHARD_TYPE" == "test" ]]
then
	send_chat DATA_BUILD "" "*sheet_id.bin*: Done" ":book:" "#00FF00"
fi
