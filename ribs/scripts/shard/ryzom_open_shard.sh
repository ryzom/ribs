#!/bin/sh
source "$RIBS_SCRIPT_PATH/tools/utils.sh"

if [[ ! "$RIBS_GROUPS" == *":dev:"* ]]
then
	echo "No soup for you"
	exit
fi

if [[ -z "$*" ]]
then
	ribs_script "shard/ryzom_open_shard.sh" "-Open Shard"
	end_prepare
fi

if ribs_prepare $1
then
	end_prepare
fi

php "$WEB_PATH/tools/manage_shard.php" open players
