#!/bin/bash
source "$RIBS_SCRIPT_PATH/tools/utils.sh"
source "$RIBS_SCRIPT_PATH/tools/rocket.sh"
source "$RIBS_BASE_PATH/data/ryzom_prefs.sh"

script_prefs="$RIBS_BASE_PATH/data/shard_stop_prefs.sh"

if [[ ! "$RIBS_GROUPS" == *":dev:"* ]]
then
	exit
fi

if [[ ! -f $script_prefs ]]
then
	clr_red "'Stop Shard' need setup. Please cook recipe [Setup Shard] and ribs [Setup Shard Stop] before"
	exit
fi

. $script_prefs

if [[ -z "$*" ]]
then
	ribs_script "shard/ryzom_stop_shard.sh" "Stop Shard"
	end_prepare
fi

if ribs_prepare $1
then
	if [[ ! -z "$GET_CONNECTED_COMMAND" ]]
	then
		output=$($GET_CONNECTED_COMMAND)

		if [[ $? == 0 ]]
		then
			ask $output

			if [[ -z "$BROADCAST_COMMAND" ]]
			then
				select_list "kickall:Kick them all" "forget:Abort stop process"
			else
				select_list "kickall:Kick them all" "broadcast:Yes but broadcast before..." "forget:Abort stop process"
			fi
		else
			clr_brown $output
		fi
	fi
	end_prepare
fi

if [ "${OPTIONS[forget]}" == "1" ]
then
	exit
fi

clr_cyan "Lock access..."
php "$WEB_PATH/tools/manage_shard.php" lock
echo "ds_dev" > /home/nevrax/www/login/server_open_status

if [ "${OPTIONS[broadcast]}" == "1" ]
then
	if [[ ! -z "$BROADCAST_COMMAND" ]]
	then
		echo "Broadcasting..."
		timer=$($BROADCAST_COMMAND | tail -n 1)
		echo "timer = $timer"
		for i in {1..10}
		do
			echo "Broadcast $i/10"
			sleep $timer
		done
	fi
fi

php "$WEB_PATH/tools/manage_shard.php" kick_them_all
sleep 1

clr_green "Stopping Shard..."

$SHARD_PATH/tools/notify.sh ShardStopped

php "$WEB_PATH/tools/manage_shard.php" stopEgs
sleep 3

export SHARD_PATH=$SHARD_PATH
$SHARD_PATH/tools/shard stop
sleep 2

clr_green "Killing all tail..."
for process in $(ps -ef | grep "tail -f /home/nevrax/shard/logs/" | grep -v grep | awk '{print  $2}')
do
        kill -9 $process
done

sleep 1

clr_green "Killing all gdb..."
for process in $(ps -ef | grep gdb | grep "ryzom_" | grep -v grep | awk '{print  $2}')
do
	kill -9 $process
done

clr_green "Deleting core dumps..."
sleep 5
rm -f /tmp/core-ryzom*

clr_green "Cleaning schroots..."
for schroot in $(mount | grep /run/schroot/mount/ | grep /home | cut -d/ -f 7)
do
	echo $schroot;
	schroot --end-session -c $schroot
done

if [ "$BACKUP_LOGS" == "1" ]
then
	DATE=$(date +%Y-%m-%d-%T)
	mkdir -p $SHARD_PATH/backups/logs/$DATE
	cp $SHARD_PATH/logs/* $SHARD_PATH/backups/logs/$DATE
fi

clr_green "Removing logs..."
rm -f $SHARD_PATH/logs/*
rm -f $SHARD_PATH/logs/chat/chat???.log
echo "" > $SHARD_PATH/logs/chat/chat.log

send_chat SERVER_REBOOT "" "*Server*: Stopped!" ":inbox_tray:" "#FF5F5F"
