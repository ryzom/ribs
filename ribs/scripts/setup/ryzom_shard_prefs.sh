#!/bin/bash
# | ___ \|_   _|| ___ \/  ___|
# | |_/ /  | |  | |_/ /\ `--.
# |    /   | |  | ___ \ `--. \
# | |\ \ __| |__| |_/ //\__/ /
# \_| \_(_)___(_)____(_)____(_)
#
# Remote Interface to Bash Scripts
# - better with barcebue sauce -
# Copyright (C) 2019 Nuneo (ulukyn@gmail.com)
#
# This program is free software (GPLv3): read https://www.gnu.org/licenses/gpl-3.0.en.html for more details
#
# Setup ryzom prefs
#

source "$RIBS_SCRIPT_PATH/tools/utils.sh"
source "$RIBS_USER_SCRIPT_PATH/../tools/ryzom_utils.sh"

prefs="$RIBS_BASE_PATH/data/ryzom_prefs.sh"

if [[ ! "$RIBS_GROUPS" == *":admin:"* ]]
then
	echo "No soup for you"
	exit
fi

if [[ -z "$*" ]]
then
	ribs_script "setup/ryzom_shard_prefs.sh" "Setup Shard Config"
	end_prepare
fi

if ribs_prepare $1
then
	# Default values
	SHARD_PATH="/home/$USER/shard"
	BUILD_PATH="/home/$USER/builds"
	PATCH_PATH="/home/$USER/patchs"
	RYZOMCORE_PATH="/home/$USER/repos/ryzom-core"
	RYZOMDATA_PATH="/home/$USER/repos/ryzom-data"
	RYZOMSERVERDATA_PATH="/home/$USER/repos/ryzom-private-data"
	RYZOMSERVER_PATH="/home/$USER/repos/ryzom-core/ryzom/server"
#	SERVER_NEL_PATH="/home/$USER/nel"
	WEB_PATH="/home/$USER/www"
	SHARD_HOST="localhost"
	SHARD_NAME="myshard"
	SHARD_DESC="(My Own Shard)"
	SHARD_ID="501"
	SHARD_DOMAIN="ryzom_mine"
	WEB_URL="http://localhost"
	DB_HOST="localhost"
	DB_RING="ring"
	DB_USER="yubo"
	DB_PASS="$(strings /dev/urandom | grep -o '[[:alnum:]]' | head -n 15 | tr -d '\n'; echo)"
	SALT="$(strings /dev/urandom | grep -o '[[:alnum:]]' | head -n 32 | tr -d '\n'; echo)"

	if [[ ! -f $prefs ]]
	then
		clr_brown "No ryzom config... please enter your values"
	else
		clr_brown "Ryzom config found. Please update the values"
		source $prefs
	fi

	ask "Path of your Shard (where all folders and files will be created by process)"
	enter_text ShardPath "$SHARD_PATH"
	ask "Path of your Builds (where sources will be compilated)"
	enter_text BuildPath "$BUILD_PATH"
	ask "Path of Patchs (where patches will be generated. A link between patch path and www will be created)"
	enter_text PatchPath "$PATCH_PATH"
	ask "Path of Ryzom Core repository [ryzom-core] (process will clone it if needed)"
	enter_text RycomCore "$RYZOMCORE_PATH"
	ask "Path of Ryzom Open Data repository [ryzom-data] (process will clone it if needed)"
	enter_text RyzomDatas "$RYZOMDATA_PATH"
	ask "Path of Ryzom Server Data repository [ryzom-private-data or ryzom-data] (process will clone it if needed)"
	enter_text RyzomServerDatas "$RYZOMSERVERDATA_PATH"
	ask "Path of Ryzom Server Source repository [ryzom-server] (process will clone it if needed)"
	enter_text Ryzom "$RYZOMSERVER_PATH"
#	ask "Path of Nel (where Nel will be installed after the compilation)"
#	enter_text Nel "$SERVER_NEL_PATH"
	ask "Path of www files (files needed by Apache and get from [ryzom-server])"
	enter_text WebPath "$WEB_PATH"
	ask "Hostname of your Shard (localhost or shard.mydomain.com)"
	enter_text ShardHost "$SHARD_HOST"
	ask "Name of your Shard"
	enter_text ShardName "$SHARD_NAME"
	ask "Desc of your Shard"
	enter_text ShardDesc "$SHARD_DESC"
	ask "Id of your Shard"
	enter_text ShardID "$SHARD_ID"
	ask "Domain of your Shard (the domain is used into client.cfg to know what shard use)"
	enter_text ShardDomain "$SHARD_DOMAIN"
	ask "Web url of your Shard (localhost or any external domain)"
	enter_text WebUrl "$WEB_URL"
	ask "Mysql Hostname (localhost or any external domain)"
	enter_text MysqlHost "$DB_HOST"
	ask "DB ring table in your mysql database"
	enter_text DbRing "$DB_RING"
	ask "DB user (with access to ring table)"
	enter_text DbUser "$DB_USER"
	ask "DB password"
	enter_text DbPass "$DB_PASS"
	ask "Salt use on EGS"
	enter_text Salt "$SALT"
	end_prepare
fi

ShardPath="${OPTIONS[ShardPath]}"
BuildPath="${OPTIONS[BuildPath]}"
PatchPath="${OPTIONS[PatchPath]}"
RycomCore="${OPTIONS[RycomCore]}"
RyzomDatas="${OPTIONS[RyzomDatas]}"
RyzomServerDatas="${OPTIONS[RyzomServerDatas]}"
Ryzom="${OPTIONS[Ryzom]}"
Nel="${OPTIONS[Nel]}"
WebPath="${OPTIONS[WebPath]}"
ShardName="${OPTIONS[ShardName]}"
ShardHost="${OPTIONS[ShardHost]}"
ShardDesc="${OPTIONS[ShardDesc]}"
ShardID="${OPTIONS[ShardID]}"
ShardDomain="${OPTIONS[ShardDomain]}"
WebUrl="${OPTIONS[WebUrl]}"
MysqlHost="${OPTIONS[MysqlHost]}"
DbRing="${OPTIONS[DbRing]}"
DbUser="${OPTIONS[DbUser]}"
DbPass="${OPTIONS[DbPass]}"
Salt="${OPTIONS[Salt]}"

if [[ -z "$ShardPath" ]]
then
	clr_red "Error! Empty values found. Aborting..."
	exit
fi

echo "Saving your preferences into $prefs..."

echo "SHARD_PATH=\"$ShardPath\"" > $prefs
echo "BUILD_PATH=\"$BuildPath\"" >> $prefs
echo "PATCH_PATH=\"$PatchPath\"" >> $prefs
echo "RYZOMCORE_PATH=\"$RycomCore\"" >> $prefs
echo "RYZOMDATA_PATH=\"$RyzomDatas\"" >> $prefs
echo "RYZOMSERVERDATA_PATH=\"$RyzomServerDatas\"" >> $prefs
echo "RYZOMSERVER_PATH=\"$Ryzom\"" >> $prefs
echo "SERVER_NEL_PATH=\"$Nel\"" >> $prefs
echo "WEB_PATH=\"$WebPath\"" >> $prefs
echo "SHARD_NAME=\"$ShardName\"" >> $prefs
echo "SHARD_HOST=\"$ShardHost\"" >> $prefs
echo "SHARD_DESC=\"$ShardDesc\"" >> $prefs
echo "SHARD_ID=\"$ShardID\"" >> $prefs
echo "SHARD_DOMAIN=\"$ShardDomain\"" >> $prefs
echo "WEB_URL=\"$WebUrl\"" >> $prefs
echo "DB_HOST=\"$MysqlHost\"" >> $prefs
echo "DB_RING=\"$DbRing\"" >> $prefs
echo "DB_USER=\"$DbUser\"" >> $prefs
echo "DB_PASS=\"$DbPass\"" >> $prefs
echo "SALT=\"$Salt\"" >> $prefs

refresh
