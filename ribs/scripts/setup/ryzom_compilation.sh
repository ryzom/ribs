#!/bin/bash
# | ___ \|_   _|| ___ \/  ___|
# | |_/ /  | |  | |_/ /\ `--.
# |    /   | |  | ___ \ `--. \
# | |\ \ __| |__| |_/ //\__/ /
# \_| \_(_)___(_)____(_)____(_)
#
# Remote Interface to Bash Scripts
# - better with barcebue sauce -
# Copyright (C) 2019 Nuneo (ulukyn@gmail.com)
#
# This program is free software (GPLv3): read https://www.gnu.org/licenses/gpl-3.0.en.html for more details
#
# Setup ryzom compilation
#

source "$RIBS_SCRIPT_PATH/tools/utils.sh"
source "$RIBS_USER_SCRIPT_PATH/../tools/ryzom_utils.sh"

prefs="$RIBS_BASE_PATH/data/ryzom_compilation_prefs.sh"
source "$RIBS_BASE_PATH/data/ryzom_prefs.sh"

if [[ ! "$RIBS_GROUPS" == *":admin:"* ]]
then
	echo "No soup for you"
	exit
fi

if [[ "$SHARD_TYPE" == "live" ]]
then
	echo "skip"
	exit
fi


if [[ -z "$*" ]]
then
	ribs_script "setup/ryzom_compilation.sh" "Setup Compilation"
	end_prepare
fi

if ribs_prepare $1
then
	if [[ ! -f $prefs ]]
	then
		clr_red "No config... please enter your values"
		CC="/usr/bin/gcc"
		CXX="/usr/bin/g++"
		SERVER_CC="/usr/bin/gcc"
		SERVER_CXX="/usr/bin/g++"
		SERVER_CHROOT="atys"
	else
		clr_green "Config found. Please update the values"
		source $prefs
	fi

	clr_white "--- SERVER ---"
	clr_brown "Which compiler to use ?"
	ask "Please enter the path of your C compiler"
	enter_text SERVER_CC "$SERVER_CC"
	ask "Please enter the path of your C++ compiler"
	enter_text SERVER_CXX "$SERVER_CXX"

	ask "Please enter the name of the chroot (empty to don't use chroot)"
	enter_text SERVER_CHROOT "$SERVER_CHROOT"

	ask "Please enter the type of compilation (Final are recommended)"
	select_list "type_dev:Dev" "type_final:Final"


	clr_white "--- CLIENT ---"
	clr_brown "Currently the client need an old gcc version. 4.8 (in ubuntu 18.04) and 4.9 (in arch)"
	clr_brown "Which compiler to use ?"
	ask "Please enter the path of your C compiler"
	enter_text CC "$CC"
	ask "Please enter the path of your C++ compiler"
	enter_text CXX "$CXX"
	
	clr_brown "You have two ways to compile client:"
	clr_brown "1) Using the common way : your linux distribution"
	clr_brown "2) Using a chroot of another linux distribution or version"
	clr_brown "If you use a chroot, you need use a script to run in root"
	ask "What way to use ?"
	select_list "common:My Linux" "chroot:A Chrooted Linux"
	ask "Do you want an option to compile for Steam ?"
	select_list "steam:Enable Steam Option" "nosteam:No need Steam Option"
	ask "Do you want an option to compile for Windows using wine ?"
	select_list "wine:Enable Wine Option" "nowine:No need Wine Option"
	ask "Do you want an option to compile for i386 architectures"
	select_list "i386:Enable i386 Option" "noi386:No need i386 Option"
	clr_brown
	end_prepare
fi

SERVER_CC="${OPTIONS[CC]}"
SERVER_CXX="${OPTIONS[CXX]}"
SERVER_CHROOT="${OPTIONS[SERVER_CHROOT]}"

if [ "${OPTIONS[type_dev]}" == "1" ]
then
	SERVER_TYPE="dev"
else
	SERVER_TYPE="final"
fi

CC="${OPTIONS[CC]}"
CXX="${OPTIONS[CXX]}"
USE_STEAM="1"
USE_WINE="1"
USE_I386="1"
USE_CHROOT="0"

if [ "${OPTIONS[nosteam]}" == "1" ]
then
	USE_STEAM="0"
fi
if [ "${OPTIONS[nowine]}" == "1" ]
then
	USE_WINE="0"
fi
if [ "${OPTIONS[noi386]}" == "1" ]
then
	USE_I386="0"
fi
if [ "${OPTIONS[chroot]}" == "1" ]
then
	USE_CHROOT="1"
fi
echo "Saving your preferences into $prefs..."

echo "SERVER_CC=\"$SERVER_CC\"" > $prefs
echo "SERVER_CXX=\"$SERVER_CXX\"" >> $prefs
echo "SERVER_CHROOT=\"$SERVER_CHROOT\"" >> $prefs
echo "SERVER_TYPE=\"$SERVER_TYPE\"" >> $prefs
echo "CC=\"$CC\"" >> $prefs
echo "CXX=\"$CXX\"" >> $prefs
echo "USE_STEAM=\"$USE_STEAM\"" >> $prefs
echo "USE_WINE=\"$USE_WINE\"" >> $prefs
echo "USE_I386=\"$USE_I386\"" >> $prefs
echo "USE_CHROOT=\"$USE_CHROOT\"" >> $prefs



