#!/bin/bash
# | ___ \|_   _|| ___ \/  ___|
# | |_/ /  | |  | |_/ /\ `--.
# |    /   | |  | ___ \ `--. \
# | |\ \ __| |__| |_/ //\__/ /
# \_| \_(_)___(_)____(_)____(_)
#
# Remote Interface to Bash Scripts
# - better with barcebue sauce -
# Copyright (C) 2019 Nuneo (ulukyn@gmail.com)
#
# This program is free software (GPLv3): read https://www.gnu.org/licenses/gpl-3.0.en.html for more details
#
# Check Repositoris
#

source "$RIBS_SCRIPT_PATH/tools/utils.sh"
source "$RIBS_USER_SCRIPT_PATH/../tools/ryzom_utils.sh"

prefs="$RIBS_BASE_PATH/data/ryzom_prefs.sh"

if [[ ! "$RIBS_GROUPS" == *":admin:"* ]]
then
	echo "No soup for you"
	exit
fi

source $prefs

if [[ -z "$*" ]]
then
	ribs_script "setup/ryzom_check_repos.sh" "Setup Repositories"
	end_prepare
fi

if ribs_prepare $1
then

	if [[ -d "$RYZOMCORE_PATH" ]]
	then
		cd "$RYZOMCORE_PATH"
		BRANCH=$(git rev-parse --abbrev-ref HEAD)
	fi

	ask "Please enter the branch to use (one of main/*)"
	enter_text BRANCH "$BRANCH"
	
	if [[ ! -d "$RYZOMCORE_PATH" ]]
	then
		clr_red "Repository at $RYZOMCORE_PATH don't exists"
	else
		clr_brown "Repository at $RYZOMCORE_PATH will be check if are up-to-date..."
	fi

	if [[ ! -d "$RYZOMSERVER_PATH" ]]
	then
		clr_red "Repository at $RYZOMSERVER_PATH don't exists"
	else
		clr_brown "Repository at $RYZOMSERVER_PATH will be check if are up-to-date..."
	fi

	if [[ ! -d "$RYZOMDATA_PATH" ]]
	then
		clr_red "Repository at $RYZOMDATA_PATH don't exists"
	else
		clr_brown "Repository at $RYZOMDATA_PATH will be check if are up-to-date..."
	fi

	if [[ ! -d "$RYZOMSERVERDATA_PATH" ]]
	then
		clr_red "Repository at $RYZOMSERVERDATA_PATH don't exists"
	else
		clr_brown "Repository at $RYZOMSERVERDATA_PATH will be check if are up-to-date..."
	fi

	end_prepare
fi

BRANCH="${OPTIONS[BRANCH]}"
echo $BRANCH
clr_green "Checking $RYZOMCORE_PATH..."
if [[ "$(check_git_repository $RYZOMCORE_PATH -)" != "OK" ]]
then
	clr_brown "NO REPOSITORY"
	mkdir -p $(dirname $RYZOMCORE_PATH)
	cd $(dirname $RYZOMCORE_PATH)
	git clone https://gitlab.com/ryzom/ryzom-core.git $(basename $RYZOMCORE_PATH)
	git checkout $BRANCH
else
	cd $RYZOMCORE_PATH
	if [[ "$(git rev-parse --abbrev-ref HEAD)" != $BRANCH ]]
	then
		git pull
		git checkout $BRANCH
	fi
	clr_cyan "latest version!"
fi

clr_green "Checking $RYZOMSERVER_PATH..."
if [[ "$(check_git_repository $RYZOMSERVER_PATH -)" != "OK" ]]
then
	clr_brown "NO REPOSITORY"
	mkdir -p $(dirname $RYZOMSERVER_PATH)
	cd $(dirname $RYZOMSERVER_PATH)
	git clone https://gitlab.com/ryzom/ryzom-server.git $(basename $RYZOMSERVER_PATH)
else
	cd $RYZOMSERVER_PATH
	if [[ "$(git rev-parse --abbrev-ref HEAD)" != $BRANCH ]]
	then
		git pull
		git checkout $BRANCH
	fi
	clr_cyan "$RYZOMSERVER_PATH: latest version!"
fi

clr_green "Checking $RYZOMDATA_PATH..."
if [[ "$(check_git_repository $RYZOMDATA_PATH -)" != "OK" ]]
then
	clr_brown "NO REPOSITORY"
	mkdir -p $(dirname $RYZOMDATA_PATH)
	cd $(dirname $RYZOMDATA_PATH)
	git clone https://gitlab.com/ryzom/ryzom-data.git $(basename $RYZOMDATA_PATH)
	git checkout $BRANCH
else
	cd $RYZOMDATA_PATH
	if [[ "$(git rev-parse --abbrev-ref HEAD)" != $BRANCH ]]
	then
		git pull
		git checkout $BRANCH
	fi
	clr_cyan "$RYZOMDATA_PATH: latest version!"
fi

clr_green "Checking $RYZOMSERVERDATA_PATH..."
if [[ "$(check_git_repository $RYZOMSERVERDATA_PATH -)" != "OK" ]]
then
	clr_brown "NO REPOSITORY"
	mkdir -p $(dirname $RYZOMSERVERDATA_PATH)
	cd $(dirname $RYZOMSERVERDATA_PATH)
	git clone git@gitlab.com:ryzomteam/ryzom-private-data.git $(basename $RYZOMSERVERDATA_PATH)
else
	cd $RYZOMSERVERDATA_PATH
	if [[ "$(git rev-parse --abbrev-ref HEAD)" != $BRANCH ]]
	then
		git pull
		git checkout $BRANCH
	fi
	clr_cyan "$RYZOMSERVERDATA_PATH: latest version!"
fi

