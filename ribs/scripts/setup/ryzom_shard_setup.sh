#!/bin/bash
# | ___ \|_   _|| ___ \/  ___|
# | |_/ /  | |  | |_/ /\ `--.
# |    /   | |  | ___ \ `--. \
# | |\ \ __| |__| |_/ //\__/ /
# \_| \_(_)___(_)____(_)____(_)
#
# Remote Interface to Bash Scripts
# - better with barcebue sauce -
# Copyright (C) 2019 Nuneo (ulukyn@gmail.com)
#
# This program is free software (GPLv3): read https://www.gnu.org/licenses/gpl-3.0.en.html for more details
#
# Setup ryzom shard
#

source "$RIBS_SCRIPT_PATH/tools/utils.sh"
source "$RIBS_USER_SCRIPT_PATH/../tools/ryzom_utils.sh"

source "$RIBS_BASE_PATH/data/ryzom_prefs.sh"

if [[ ! "$RIBS_GROUPS" == *":admin:"* ]]
then
	echo "No soup for you"
	exit
fi

if [[ -z "$*" ]]
then
	ribs_script "setup/ryzom_shard_setup.sh" "Setup Shard"
	end_prepare
fi

global_conf="$SHARD_PATH/globals.cfg"

if ribs_prepare $1
then

	if [ ! -d "$WEB_PATH" ]
	then
		clr_brown "$WEB_PATH not found. Will be created"
	fi

	if [ ! -d "$WEB_PATH/login" ]
	then
		clr_brown "$WEB_PATH/login not found. Will be linked to $RYZOMSERVER_PATH/www/login"
	fi

	if [ ! -f "$WEB_PATH/login/config.php" ]
	then
		clr_brown "$WEB_PATH/login/config.php not found. Will be generated"
	fi

	if [[ ! -f "$global_conf" ]]
	then
		clr_brown "$global_conf not found. Will be generated"
	fi

	if [[ ! -d "$SHARD_PATH/cfgs" ]]
	then
		clr_brown "$SHARD_PATH/cfgs/ not found. Will be generated"
	fi

	if [[ ! -L "$SHARD_PATH/common" ]]
	then
		clr_brown "$SHARD_PATH/common not found. Will be generated"
	fi
	end_prepare
fi

if [ ! -d "$WEB_PATH" ]
then
	mkdir "$RYZOMSERVER_PATH/www"
fi

if [ ! -d "$WEB_PATH/login" ]
then
	ln -s "$RYZOMSERVER_PATH/www/login" "$WEB_PATH/login"
fi

if [ ! -f "$WEB_PATH/login/config.php" ]
then
	color orange "Generating web config... "
	cp "$WEB_PATH/login/config_example.php" "$WEB_PATH/login/config.php"
	sed -i -e "s/DB_HOST/$DB_HOST/" "$WEB_PATH/login/config.php"
	sed -i -e "s/RING_DB/$DB_RING/" "$WEB_PATH/login/config.php"
	sed -i -e "s/RING_USER/$DB_USER/" "$WEB_PATH/login/config.php"
	sed -i -e "s/RING_PASS/$DB_PASS/" "$WEB_PATH/login/config.php"
	sed -i -e "s/USER/$DB_USER/" "$WEB_PATH/login/config.php"
	sed -i -e "s/PASSWORD/$DB_PASS/" "$WEB_PATH/login/config.php"
	clr_green "Done!"
fi

if [[ ! -f "$global_conf" ]]
then
	mkdir -p $SHARD_PATH
	ShardUCName="$(tr '[:lower:]' '[:upper:]' <<< ${SHARD_NAME:0:1})${SHARD_NAME:1}"

	echo "Saving your preferences into $prefs..."

	echo "#SHARDNAME# = $SHARD_NAME" > $global_conf
	echo "#UC_SHARDNAME# = $ShardUCName" >> $global_conf
	echo "#SHARDDESC# = $SHARD_DESC" >> $global_conf
	echo "#SHARDID# = $SHARD_ID" >> $global_conf
	echo "#DOMAINNAME# = $SHARD_DOMAIN" >> $global_conf
	echo "#WEBHOST# = $WEB_URL" >> $global_conf
	echo "#HOSTNAME# = $SHARD_HOST" >> $global_conf
	echo "#DBHOST# = $DB_HOST" >> $global_conf
	echo "#DBRING# = $DB_RING" >> $global_conf
	echo "#DBUSER# = $DB_USER" >> $global_conf
	echo "#DBPASS# = $DB_PASS" >> $global_conf
fi

# Creating the base of the shard
mkdir -p "$SHARD_PATH/cache"
mkdir -p "$SHARD_PATH/run"
mkdir -p "$SHARD_PATH/logs"
mkdir -p "$SHARD_PATH/save/rrd_graphs"

if [[ ! -d "$SHARD_PATH/cfgs" ]]
then
	if [[ -f "$RYZOMSERVER_PATH/tools/cfg_creator/create_cfgs.py" ]]
	then
		mkdir -p "$SHARD_PATH/cfgs"
		"$RIBS_BASE_PATH/server/env/bin/python" "$RYZOMSERVER_PATH/tools/cfg_creator/create_cfgs.py" "$SHARD_PATH"
	else
		clr_brown "NO REPOSITORY $RYZOMSERVER_PATH/tools/cfg_creator/create_cfgs.py found to generate cfgs files"
		exit 1
	fi
fi

if [[ ! -L "$SHARD_PATH/common" ]]
then
	ln -s "$RYZOMDATA_PATH/common" "$SHARD_PATH"
	ln -s "$RYZOMSERVERDATA_PATH/common/data_leveldesign" "$SHARD_PATH/common/"
fi

if [[ ! -d "$SHARD_PATH/language" ]]
then
	mkdir -p "$SHARD_PATH/language/phrases"
	ln -s "$RYZOMSERVERDATA_PATH"/tools/translation/translated/phrase_* "$SHARD_PATH/language/phrases/"
	ln -s "$RYZOMDATA_PATH/translations/translated" "$SHARD_PATH/language/"
fi

if [[ ! -d "$SHARD_PATH/tools" ]]
then
	ln -s "$RIBS_USER_SCRIPT_PATH/../tools" "$SHARD_PATH/"
fi

if [[ ! -d "$SHARD_PATH/data" ]]
then
	mkdir -p "$SHARD_PATH/data/leveldesign/primitive_cache"
	cp "$RYZOMSERVERDATA_PATH/data/client_commands_privileges.txt.default" "$SHARD_PATH/data/client_commands_privileges.txt"
	ln -s "$RYZOMSERVERDATA_PATH/data/brick_sheets" "$SHARD_PATH/data"
	ln -s "$RYZOMSERVERDATA_PATH/data/dev_gm_names.xml" "$SHARD_PATH/data"
	ln -s "$RYZOMSERVERDATA_PATH/data/game_event.txt" "$SHARD_PATH/data"
	ln -s "$RYZOMSERVERDATA_PATH/data/init_episode2_database.cmd" "$SHARD_PATH/data"
	ln -s "$RYZOMSERVERDATA_PATH/data/invalid_entity_names.txt" "$SHARD_PATH/data"
	ln -s "$RYZOMSERVERDATA_PATH/data/mirror_sheets" "$SHARD_PATH/data"
	ln -s "$RYZOMSERVERDATA_PATH/data/mission_queues.txt" "$SHARD_PATH/data"
	ln -s "$RYZOMSERVERDATA_PATH/data/named_items.txt" "$SHARD_PATH/data"
	ln -s "$RYZOMSERVERDATA_PATH/data/newbie_guilds.xml" "$SHARD_PATH/data"
	ln -s "$RYZOMSERVERDATA_PATH/data/position_flags.xml" "$SHARD_PATH/data"
	ln -s "$RYZOMSERVERDATA_PATH/data/reserved_names.xml" "$SHARD_PATH/data"
	ln -s "$RYZOMSERVERDATA_PATH/data/shop_category.cfg" "$SHARD_PATH/data"
	ln -s "$RYZOMSERVERDATA_PATH/data/single.property_array" "$SHARD_PATH/data"
fi

if [[ ! -e ~/bin/shard ]]
then
	mkdir -p ~/bin
	ln -s $SHARD_PATH/tools/shard ~/bin
fi

if [[ ! $(wget -S --spider "http://$SHARD_HOST:40916/login/tools/check_access.php"  2>&1 | grep 'HTTP/1.1 200 OK') ]]
then

	clr_brown  "Can't access to <a style='color: orange' href='http://$SHARD_HOST:40916/login/tools/check_access.php'>http://$SHARD_HOST:40916/login/tools/check_access.php</a><br />"
	clr_cyan "Please check your apache2 installation."
	clr_cyan "You must create a virtualhost config file for ryzom : "
	input "nano /etc/apache2/sites-available/login.conf"
	clr_cyan "With content:"
	read -r -d '' VH << EOM
<VirtualHost *:40916>\n\
<Directory \"$WEB_PATH\">\n\
Options Indexes FollowSymLinks\n\
AllowOverride None\n\
Require all granted\n\
</Directory>\n\
\n\
ServerName $SHARD_HOST\n\
ServerAdmin admin@localhost\n\
DocumentRoot $WEB_PATH\n\
ErrorLog ${APACHE_LOG_DIR}/error.log\n\
CustomLog ${APACHE_LOG_DIR}/access.log combined\n\
</VirtualHost>\n
EOM

	text $VH
	echo " "
	clr_cyan "Don't forget to add:"
	input "Listen 40916"
	clr_cyan "to:"
	input "nano /etc/apache2/ports.conf"
	clr_cyan "and run:"
	input "a2ensite login.conf; service apache2 reload"
	end_prepare
fi

WGETOUT=$(wget -qO- http://$SHARD_HOST:40916/login/tools/check_access.php)
if [[ "$WGETOUT" == "DBERR" ]]
then
	clr_cyan "Please open mysql console (with root or another priviledged account):"
	input "mysql -u root -p"
	clr_cyan "run theses commandes in mysql console:"
	read -r -d '' SQL << EOM
CREATE DATABASE nel;\n\
CREATE DATABASE $DB_RING;\n\
CREATE USER '$DB_USER'@'localhost' IDENTIFIED BY '$DB_PASS';\n\
GRANT ALL PRIVILEGES ON nel.* TO '$DB_USER'@'localhost';\n\
GRANT ALL PRIVILEGES ON $DB_RING.* TO '$DB_USER'@'localhost';\n
EOM
	text $SQL
	clr_cyan "Init the database with commands:"
	input "mysql -u $DB_USER -p$DB_PASS nel < \"$WEB_PATH/login/nel.sql\""
	clr_cyan "and"
	input "mysql -u $DB_USER -p$DB_PASS $DB_RING < \"$WEB_PATH/login/ring.sql\""
	end_prepare
fi

if [[ "$WGETOUT" == "NOD" ]]
then
	cd "$WEB_PATH/login/tools"
	OUT=$(php shard_init.php "$SHARD_DOMAIN" "$SHARD_HOST" "$DB_RING" "$SHARD_DESC" "$SHARD_ID" "$SHARD_NAME")
	if [[ "$OUT" == "OK" ]]
	then
		touch "$RIBS_BASE_PATH/data/www_access_checked"
		clr_green "Checking access... Ok"
	fi
fi

if [[ "$WGETOUT" == "AOK" ]]
then
	clr_cyan "Database already configured!"
fi

clr_cyan "Packages:"
# download extra packages
if [ ! -d "$SHARD_PATH/data/r2_islands" ]
then
	checkPackage "r2_islands"
else
	echo "-> r2_islands"
fi

if [ ! -d "$SHARD_PATH/data/collisions" ]
then
	checkPackage "collisions"
else
	echo "-> collisions"
fi
rm -f SHA256
# init now
cd - >/dev/null
touch "$initialized" && clr_cyan "Done!"
