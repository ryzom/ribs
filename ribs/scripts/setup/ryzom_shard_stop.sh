#!/bin/bash
# | ___ \|_   _|| ___ \/  ___|
# | |_/ /  | |  | |_/ /\ `--.
# |    /   | |  | ___ \ `--. \
# | |\ \ __| |__| |_/ //\__/ /
# \_| \_(_)___(_)____(_)____(_)
#
# Remote Interface to Bash Scripts
# - better with barcebue sauce -
# Copyright (C) 2019 Nuneo (ulukyn@gmail.com)
#
# This program is free software (GPLv3): read https://www.gnu.org/licenses/gpl-3.0.en.html for more details
#
# Setup ryzom stop shard
#

source "$RIBS_SCRIPT_PATH/tools/utils.sh"
source "$RIBS_USER_SCRIPT_PATH/../tools/ryzom_utils.sh"

prefs="$RIBS_BASE_PATH/data/shard_stop_prefs.sh"

if [[ ! "$RIBS_GROUPS" == *":admin:"* ]]
then
	echo "No soup for you"
	exit
fi

source "$RIBS_BASE_PATH/data/ryzom_prefs.sh"

if [[ -z "$*" ]]
then
	ribs_script "setup/ryzom_shard_stop.sh" "Setup Shard Stop"
	end_prepare
fi

if ribs_prepare $1
then
	if [[ ! -f $prefs ]]
	then
		clr_brown "No config... please enter your values"
		GET_CONNECTED_COMMAND="php $WEB_PATH/tools/get_connected_players.php"
		BROADCAST_COMMAND="php $WEB_PATH/tools/broadcast.php dev"
		BACKUP_LOGS="0"
	else
		clr_brown "Config found. Please update the values"
		source $prefs
	fi

	ee=$(escape "$GET_CONNECTED_COMMAND")
	echo "[[$ee]]"
	ask "Command to display who are connected to shard and ask for continue, broadcast or abort the stop process"
	enter_text GET_CONNECTED_COMMAND "$GET_CONNECTED_COMMAND"
	ask "Broadcast command used to prevent connected players"
	enter_text BROADCAST_COMMAND "$BROADCAST_COMMAND"
	ask "Keep logs? (current choice : $BACKUP_LOGS)"
	select_list "backuplogs:Yes" "nobackup:No"
	end_prepare
fi

if [ "${OPTIONS[backuplogs]}" == "1" ]
then
	BACKUP_LOGS="1"
else
	BACKUP_LOGS="0"
fi

GET_CONNECTED_COMMAND=$(escape "${OPTIONS[GET_CONNECTED_COMMAND]}")
BROADCAST_COMMAND=$(escape "${OPTIONS[BROADCAST_COMMAND]}")

echo "Saving your preferences into $prefs..."

echo "GET_CONNECTED_COMMAND=\"$GET_CONNECTED_COMMAND\"" > $prefs
echo "BROADCAST_COMMAND='$BROADCAST_COMMAND'" >> $prefs
echo "BACKUP_LOGS='$BACKUP_LOGS'" >> $prefs
