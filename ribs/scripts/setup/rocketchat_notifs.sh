#!/bin/bash

source "$RIBS_SCRIPT_PATH/tools/utils.sh"
source "$RIBS_BASE_PATH/data/ryzom_prefs.sh"

prefs="$RIBS_BASE_PATH/data/rocketchat_prefs.sh"


if [[ ! "$RIBS_GROUPS" == *":admin:"* ]]
then
	echo "No soup for you"
	exit
fi

if [[ -z "$*" ]]
then
	ribs_script "setup/rocketchat_notifs.sh" "Setup RocketChat Notifs"
	end_prepare
fi

if ribs_prepare $1
then
	RC_SERVER="$WEB_URL"
	CLIENT_BUILD_TOKEN="1234"
	CLIENT_PATCH_TOKEN="1234"
	DATA_BUILD_TOKEN="1234"
	SERVER_BUILD_TOKEN="1234"
	SERVER_REBOOT_TOKEN="1234"
	if [[ ! -f $prefs ]]
	then
		clr_red "No config... please enter your values"
	else
		clr_green "Config found. Please update the values"
		source $prefs
	fi

	ask "Please enter the RocketChat server url"
	enter_text RC_SERVER "$RC_SERVER"

	ask "Please enter the RocketChat token for Client Builds"
	enter_text CLIENT_BUILD_TOKEN "$CLIENT_BUILD_TOKEN"

	ask "Please enter the RocketChat token for Client Patches"
	enter_text CLIENT_PATCH_TOKEN "$CLIENT_PATCH_TOKEN"

	ask "Please enter the RocketChat token for Client Data Builds"
	enter_text DATA_BUILD_TOKEN "$DATA_BUILD_TOKEN"

	ask "Please enter the RocketChat token for Server Builds"
	enter_text SERVER_BUILD_TOKEN "$SERVER_BUILD_TOKEN"

	ask "Please enter the RocketChat token for Server Reboot"
	enter_text SERVER_REBOOT_TOKEN "$SERVER_REBOOT_TOKEN"

	end_prepare
fi

RC_SERVER="${OPTIONS[RC_SERVER]}"
CLIENT_BUILD_TOKEN="${OPTIONS[CLIENT_BUILD_TOKEN]}"
CLIENT_PATCH_TOKEN="${OPTIONS[CLIENT_PATCH_TOKEN]}"
DATA_BUILD_TOKEN="${OPTIONS[DATA_BUILD_TOKEN]}"
SERVER_BUILD_TOKEN="${OPTIONS[SERVER_BUILD_TOKEN]}"
SERVER_REBOOT_TOKEN="${OPTIONS[SERVER_REBOOT_TOKEN]}"

echo "Saving your preferences into $prefs..."

echo "RC_SERVER=\"$RC_SERVER\"" > $prefs
echo "CLIENT_BUILD_TOKEN=\"$CLIENT_BUILD_TOKEN\"" >> $prefs
echo "CLIENT_PATCH_TOKEN=\"$CLIENT_PATCH_TOKEN\"" >> $prefs
echo "DATA_BUILD_TOKEN=\"$DATA_BUILD_TOKEN\"" >> $prefs
echo "SERVER_BUILD_TOKEN=\"$SERVER_BUILD_TOKEN\"" >> $prefs
echo "SERVER_REBOOT_TOKEN=\"$SERVER_REBOOT_TOKEN\"" >> $prefs

