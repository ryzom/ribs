#!/bin/bash

source "$RIBS_SCRIPT_PATH/tools/utils.sh"
source "$RIBS_BASE_PATH/data/ryzom_prefs.sh"

if [[ ! "$RIBS_GROUPS" == *":gamedesigner:"* ]]
then
	exit
fi

STAT="Dmg"
CACHE="$RIBS_BASE_PATH/data/shard_stats/egs_val_$STAT"
mkdir -p $CACHE

if [[ -z "$*" ]]
then
	ribs_script "server_data/change_damage_rangeweapons.sh" "Change Range Weapon $STAT"
	end_prepare
fi

if ribs_prepare $1
then

	ask "Please change the $STAT:"
	newline

	for weapon in Autolauch Bowrifle Launcher Pistol Bowpistol Rifle
	do
		if [ ! -f "$CACHE/${weapon}.txt" ]
		then
			val=$(php $WEB_PATH/login/tools/getfromshard.php "${weapon}$STAT" | cut -d" " -f4)
			valmax=$(php $WEB_PATH/login/tools/getfromshard.php "${weapon}${STAT}Max" | cut -d" " -f4)
		else
			val=$(cat "$CACHE/${weapon}.txt")
			valmax=$(cat "$CACHE/${weapon}_max.txt")
		fi
		echo -n "(>text)"
		clr_brown "${weapon} $STAT = $val - $valmax"
		enter_text "${weapon}$STAT" "$val"
		enter_text "${weapon}${STAT}Max" "$valmax"
	done
	end_prepare
fi

for weapon in Autolauch Bowrifle Launcher Pistol Bowpistol Rifle
do
	WSTAT="${weapon}$STAT"
	WSTATMAX="${weapon}${STAT}Max"
	I=${OPTIONS[$WSTAT]}
	J=${OPTIONS[$WSTATMAX]}
	if [ ! -z $I ] && [ ! -z $J ]
	then
		echo "Applying... ${weapon} $STAT = $I - $J"
		echo "$I" > "$CACHE/${weapon}.txt"
		echo "$J" > "$CACHE/${weapon}_max.txt"
		php $WEB_PATH/tools/sendtoshard.php "${weapon}$STAT" "$I"
		php $WEB_PATH/tools/sendtoshard.php "${weapon}${STAT}Max" "$J"
	fi
done

FILENAME=$(date +%Y-%m-%d_%H%M%S)
mkdir -p $CACHE/backup/$FILENAME
cp $CACHE/*.txt $CACHE/backup/$FILENAME
