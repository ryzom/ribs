#!/bin/bash

source "$RIBS_SCRIPT_PATH/tools/utils.sh"
source "$RIBS_BASE_PATH/data/ryzom_prefs.sh"

if [[ ! "$RIBS_GROUPS" == *":gamedesigner:"* ]]
then
	exit
fi

STAT="Range"
CACHE="$RIBS_BASE_PATH/data/shard_stats/egs_val_$STAT"
mkdir -p $CACHE

if [[ -z "$*" ]]
then
	ribs_script "server_data/change_range_values.sh" "Change Range Weapon/Ammo $STAT"
	end_prepare
fi

if ribs_prepare $1
then

	ask "Please change the range:"
	newline

	for weapon in Autolauch Bowrifle Launcher Pistol Bowpistol Rifle AutolaunchAmmo BowrifleAmmo LauncherAmmo PistolAmmo BowpistolAmmo RifleAmmo
	do
		if [ ! -f "$CACHE/${weapon}.txt" ]
		then
			val=$(php $WEB_PATH/login/tools/getfromshard.php "${weapon}$STAT" | cut -d" " -f4)
		else
			val=$(cat "$CACHE/${weapon}.txt")
		fi

		echo -n "(>text)"
		clr_brown "${weapon} $STAT = $val"
		enter_text "${weapon}$STAT" "$val"
	done
	end_prepare
fi


for weapon in Autolauch Bowrifle Launcher Pistol Bowpistol Rifle AutolaunchAmmo BowrifleAmmo LauncherAmmo PistolAmmo BowpistolAmmo RifleAmmo
do
	WSTAT="${weapon}$STAT"
	I=${OPTIONS[$WSTAT]}
	if [ ! -z $I ]
	then
		echo "Applying... ${weapon} $STAT = $I"
		echo "$I" > "$CACHE/${weapon}.txt"
		php $WEB_PATH/tools/sendtoshard.php "${weapon}$STAT" "$I"
	fi
done

mkdir -p $CACHE/backup
FILENAME=$(date +%Y-%m-%d_%R:%S)
tar czf $CACHE/backup/$FILENAME.tgz $CACHE/*.txt

