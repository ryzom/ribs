#!/bin/bash
#############################################
# | ___ \|_   _|| ___ \/  ___|
# | |_/ /  | |  | |_/ /\ `--.
# |    /   | |  | ___ \ `--. \
# | |\ \ __| |__| |_/ //\__/ /
# \_| \_(_)___(_)____(_)____(_)
#
# Remote Interface to Bash Scripts
# - better with barcebue sauce -
# Copyright (C) 2019 Nuneo (ulukyn@gmail.com)
#
# This program is free software (GPLv3): read https://www.gnu.org/licenses/gpl-3.0.en.html for more details
#
# Ryzom build Server Script
#

source "$RIBS_SCRIPT_PATH/tools/utils.sh"
source "$RIBS_BASE_PATH/data/ryzom_prefs.sh"
source "$RIBS_USER_PATH/tools/ryzom_utils.sh"

prefs="$RIBS_BASE_PATH/data/ryzom_compilation_prefs.sh"

if [[ ! "$RIBS_GROUPS" == *":dev:"* ]]
then
	exit
fi

if [[ -f "$prefs" ]]
then
	source "$prefs"
else
	clr_red "Compilation of server need be setuped before"
	clr_brown "Please use Recipe : Setup Shard and Ribs : Setup Compilation"
	exit
fi

if [[ -z "$*" ]]
then
	ribs_script "build/ryzom_build_server.sh" "Build Server" "Nel"
	end_prepare
fi

if [ "$1" == "stop" ]
then
	if [ ! -z "$SERVER_CHROOT" ]
	then
		cat $RIBS_USER_PATH/data/build_server_chroot.session
		CHROOT_SESSION=$(cat $RIBS_USER_PATH/data/build_server_chroot.session)
		if [[ ! -z "$CHROOT_SESSION" ]]
		then
			schroot --end-session -c "$CHROOT_SESSION"
		fi
	fi
	exit
fi


if ribs_prepare $1
then
	check_git_repository $RYZOMSERVER_PATH UpdateServerRepo
	end_prepare
fi

CC="$SERVER_CC"
CXX="$SERVER_CXX"

#Update repositories
if [ "${OPTIONS[UpdateServerRepo]}" == "1" ]
then
	cd $RYZOMSERVER_PATH
	git pull
fi

SBIN_PATH=$SHARD_PATH/sbin/


xsltproc  --stringparam filename database_mapping --stringparam output header  --output $RYZOMSERVER_PATH/src/shard_unifier_service/database_mapping.h.tmp $RYZOMCORE_PATH/ryzom/common/src/game_share/generate_module_interface.xslt $RYZOMSERVER_PATH/src/shard_unifier_service/nel_database_mapping.xml
xsltproc  --stringparam filename database_mapping --stringparam output cpp     --output $RYZOMSERVER_PATH/src/shard_unifier_service/database_mapping.cpp.tmp $RYZOMCORE_PATH/ryzom/common/src/game_share/generate_module_interface.xslt $RYZOMSERVER_PATH/src/shard_unifier_service/nel_database_mapping.xml

xsltproc --stringparam filename database --stringparam bank PLR --stringparam output header --stringparam side server --output $RYZOMSERVER_PATH/src/entities_game_service/database_plr.h.tmp $RYZOMCORE_PATH/ryzom/common/src/game_share/generate_client_db.xslt $RYZOMCORE_PATH/ryzom/common/data_common/database.xml
xsltproc --stringparam filename database --stringparam bank PLR --stringparam output cpp --stringparam side server --output $RYZOMSERVER_PATH/src/entities_game_service/database_plr.cpp.tmp $RYZOMCORE_PATH/ryzom/common/src/game_share/generate_client_db.xslt $RYZOMCORE_PATH/ryzom/common/data_common/database.xml

xsltproc --stringparam filename database --stringparam bank GUILD --stringparam output header --stringparam side server --output $RYZOMSERVER_PATH/src/entities_game_service/database_guild.h.tmp $RYZOMCORE_PATH/ryzom/common/src/game_share/generate_client_db.xslt $RYZOMCORE_PATH/ryzom/common/data_common/database.xml
xsltproc --stringparam filename database --stringparam bank GUILD --stringparam output cpp --stringparam side server --output $RYZOMSERVER_PATH/src/entities_game_service/database_guild.cpp.tmp $RYZOMCORE_PATH/ryzom/common/src/game_share/generate_client_db.xslt $RYZOMCORE_PATH/ryzom/common/data_common/database.xml

xsltproc --stringparam filename guild_unifier_itf --stringparam bank GUILD --stringparam output cpp --stringparam side server --output  $RYZOMSERVER_PATH/src/entities_game_service/guild_manager/guild_unifier_itf.cpp.tmp $RYZOMCORE_PATH/ryzom/common/src/game_share/generate_module_interface.xslt $RYZOMSERVER_PATH/src/entities_game_service/guild_manager/guild_unifier_itf.xml
xsltproc --stringparam filename guild_unifier_itf --stringparam bank GUILD --stringparam output header --stringparam side server --output  r$RYZOMSERVER_PATH/src/entities_game_service/guild_manager/guild_unifier_itf.h.tmp $RYZOMCORE_PATH/ryzom/common/src/game_share/generate_module_interface.xslt $RYZOMSERVER_PATH/src/entities_game_service/guild_manager/guild_unifier_itf.xml


DIFF_DB_MAPPING_H=$(diff -w $RYZOMSERVER_PATH/src/shard_unifier_service/database_mapping.h.tmp $RYZOMSERVER_PATH/src/shard_unifier_service/database_mapping.h)
DIFF_DB_MAPPING_CPP=$(diff -w $RYZOMSERVER_PATH/src/shard_unifier_service/database_mapping.cpp.tmp $RYZOMSERVER_PATH/src/shard_unifier_service/database_mapping.cpp)

DIFF_DB_PLR_H=$(diff -w $RYZOMSERVER_PATH/src/entities_game_service/database_plr.h.tmp $RYZOMSERVER_PATH/src/entities_game_service/database_plr.h)
DIFF_DB_PLR_CPP=$(diff -w $RYZOMSERVER_PATH/src/entities_game_service/database_plr.cpp.tmp $RYZOMSERVER_PATH/src/entities_game_service/database_plr.cpp)

DIFF_DB_GUILD_H=$(diff -w $RYZOMSERVER_PATH/src/entities_game_service/database_guild.h.tmp $RYZOMSERVER_PATH/src/entities_game_service/database_guild.h)
DIFF_DB_GUILD_CPP=$(diff -w $RYZOMSERVER_PATH/src/entities_game_service/database_guild.cpp.tmp $RYZOMSERVER_PATH/src/entities_game_service/database_guild.cpp)

DIFF_GUILD_UNIFIER_H=$(diff -w $RYZOMSERVER_PATH/src/entities_game_service/guild_manager/guild_unifier_itf.h.tmp $RYZOMSERVER_PATH/src/entities_game_service/guild_manager/guild_unifier_itf.h)
DIFF_GUILD_UNIFIER_CPP=$(diff -w $RYZOMSERVER_PATH/src/entities_game_service/guild_manager/guild_unifier_itf.cpp.tmp $RYZOMSERVER_PATH/src/entities_game_service/guild_manager/guild_unifier_itf.cpp)


if [ ! -z "$DIFF_DB_MAPPING_H" ] || [ ! -z "$DIFF_DB_MAPPING_CPP" ]
then
	clr_green "Generating database_guild..."
	cp $RYZOMSERVER_PATH/src/shard_unifier_service/database_mapping.h.tmp $RYZOMSERVER_PATH/src/shard_unifier_service/database_mapping.h
	cp $RYZOMSERVER_PATH/src/shard_unifier_service/database_mapping.h.tmp $RYZOMSERVER_PATH/src/shard_unifier_service/database_mapping.h
fi

if [ ! -z "$DIFF_DB_PLR_H" ] || [ ! -z "$DIFF_DB_PLR_CPP" ]
then
	clr_green "Generating database_plr..."
	cp $RYZOMSERVER_PATH/src/entities_game_service/database_plr.h.tmp $RYZOMSERVER_PATH/src/entities_game_service/database_plr.h
	cp $RYZOMSERVER_PATH/src/entities_game_service/database_plr.cpp.tmp $RYZOMSERVER_PATH/src/entities_game_service/database_plr.cpp
fi

if [ ! -z "$DIFF_DB_GUILD_H" ] || [ ! -z "$DIFF_DB_GUILD_CPP" ]
then
	clr_green "Generating database_guild..."
	cp $RYZOMSERVER_PATH/src/entities_game_service/database_guild.h.tmp $RYZOMSERVER_PATH/src/entities_game_service/database_guild.h
	cp $RYZOMSERVER_PATH/src/entities_game_service/database_guild.cpp.tmp $RYZOMSERVER_PATH/src/entities_game_service/database_guild.cpp
fi

if [ ! -z "$DIFF_GUILD_UNIFIER_H" ] || [ ! -z "$DIFF_GUILD_UNIFIER_CPP" ]
then
	clr_green "Generating guild_unifier_itf..."
	cp $RYZOMSERVER_PATH/src/entities_game_service/guild_manager/guild_unifier_itf.h.tmp $RYZOMSERVER_PATH/src/entities_game_service/guild_manager/guild_unifier_itf.h
	cp $RYZOMSERVER_PATH/src/entities_game_service/guild_manager/guild_unifier_itf.cpp.tmp $RYZOMSERVER_PATH/src/entities_game_service/guild_manager/guild_unifier_itf.cpp
fi

# xsltproc  --output msg_ais_egs_gen.h generate_module_interface.xslt msg_ais_egs_gen.xml

TYPE=$SERVER_TYPE
PROC=$(nproc)

clr_cyan "Compilation params : (-j$PROC) $SHARD_NAME $TYPE"

NEL_PATH=$SERVER_NEL_PATH/${SHARD_NAME}_${TYPE}

if [[ -d $RYZOMCORE_PATH/code ]]
then
	RYZOMCORE_CODE_PATH=$RYZOMCORE_PATH/code
else
	RYZOMCORE_CODE_PATH=$RYZOMCORE_PATH
fi

if [ "${OPTIONS[Nel]}" == "1" ]
then
	DIR=$BUILD_PATH/nel/${SHARD_NAME}_${TYPE}
	mkdir -p $DIR
	cd $DIR

	clr_green "Runing Cmake for Nel/GameShare from $RYZOMCORE_PATH into $DIR..."

	if [ ! -z "$SERVER_CHROOT" ]
	then
		schroot -c $SERVER_CHROOT -- cmake $RYZOMCORE_CODE_PATH -DRYZOM_PRODUCT_VERSION="1.0.0" -DPLATFORM_CXXFLAGS="--param ggc-min-expand=1 --param ggc-min-heapsize=512000" -DCMAKE_C_COMPILER=$CC -DCMAKE_CXX_COMPILER=$CXX -DWITH_RYZOM=ON -DWITH_RYZOM_SERVER=OFF -DWITH_NEL_TOOLS=OFF -DWITH_RYZOM_TOOLS=OFF -DWITH_NEL_SAMPLES=OFF -DWITH_3D=OFF -DWITH_SOUND=OFF -DWITH_RYZOM_CLIENT=OFF -DWITH_PCH=OFF -DWITH_NEL_TESTS=OFF -DWITH_GUI=OFF -DWITH_STATIC=OFF -DCMAKE_INSTALL_PREFIX=$NEL_PATH
		clr_green "Compiling..."
		schroot -c $SERVER_CHROOT -- make -j$PROC
		clr_green "Instaling..."
		schroot -c $SERVER_CHROOT -- make DESTDIR= install
	else
		cmake $RYZOMCORE_CODE_PATH -DCMAKE_C_COMPILER=$CC -DCMAKE_CXX_COMPILER=$CXX -DWITH_RYZOM=ON -DWITH_RYZOM_SERVER=OFF -DWITH_NEL_TOOLS=OFF -DWITH_RYZOM_TOOLS=OFF -DWITH_NEL_SAMPLES=OFF -DWITH_3D=OFF -DWITH_SOUND=OFF -DWITH_RYZOM_CLIENT=OFF -DWITH_PCH=OFF -DWITH_NEL_TESTS=OFF -DWITH_GUI=OFF -DWITH_STATIC=OFF -DCMAKE_INSTALL_PREFIX=$NEL_PATH
		clr_green "Compiling..."
		make -j$PROC || exit
		clr_green "Instaling..."
		make DESTDIR= install
	fi
	clr_cyan "Ok"
fi

DIR=$BUILD_PATH/server/${SHARD_NAME}_${TYPE}
mkdir -p $DIR
cd $DIR


CMAKEFLAGS="-DCMAKE_C_COMPILER=$CC \
	-DCMAKE_CXX_COMPILER=$CXX \
	-DCMAKE_BUILD_TYPE=Release \
	-DWITH_NEL=ON \
	-DWITH_RYZOM_SERVER=ON \
	-DWITH_RYZOM_GAMESHARE=ON \
	-DWITH_SYMBOLS=ON \
	-DWITH_RYZOM_TOOLS=OFF \
	-DWITH_RYZOM_CLIENT=OFF \
	-DWITH_NEL_TESTS=OFF \
	-DWITH_NEL_TOOLS=OFF \
	-DWITH_NEL_SAMPLES=OFF \
	-DWITH_UNIX_STRUCTURE=OFF \
	-DWITH_INSTALL_LIBRARIES=OFF \
	-DWITH_DRIVER_OPENGL=OFF \
	-DWITH_DRIVER_OPENAL=OFF \
	-DWITH_PCH=OFF \
	-DWITH_STATIC=ON \
	-DWITH_GUI=OFF \
	-DWITH_3D=OFF \
	-DWITH_SOUND=OFF \
"


clr_green "Runing CMake for Server from $RYZOMCORE_CODE_PATH..."
echo "$SERVER_CHROOT";
if [ ! -z "$SERVER_CHROOT" ]
then
	SESSION_FILE="$RIBS_BASE_PATH/data/build_server_chroot.session"

	CHROOT_SESSION=$(schroot --begin-session -c $SERVER_CHROOT)
	echo "$CHROOT_SESSION"
	echo "$CHROOT_SESSION" > "$SESSION_FILE"

	clr_cyan "Starting chroot session $CHROOT_SESSION"

	schroot --run-session -c "$CHROOT_SESSION" -- cmake $RYZOMCORE_CODE_PATH -DPLATFORM_CXXFLAGS="--param ggc-min-expand=1 --param ggc-min-heapsize=512000" $CMAKEFLAGS
	check_success
	clr_green "Compiling..."
	schroot --run-session -c "$CHROOT_SESSION" -- make -j$PROC
	check_success "$CHROOT_SESSION"
	echo "" > "$SESSION_FILE"
else
	echo "NO CHROOT"
	cmake $RYZOMSERVER_PATH $CMAKEFLAGS
	check_success
	clr_green "Compiling..."
	make -j$PROC
	check_success
fi

clr_green "Instaling from $(pwd)/bin to $SBIN_PATH..."
rsync -av bin/ $SBIN_PATH


# Clean chroot sessions
# GINGO
#for a in $(mount | grep /run/schroot/mount/ | grep /home | cut -d/ -f 7); do echo $a;schroot --end-session -c $a;done
# YUBO
#for a in $(mount | grep /run/schroot/mount/ | grep /home | cut -d/ -f 7); do echo $a; schroot --end-session -c $a;done

