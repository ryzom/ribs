#!/bin/bash
source "$RIBS_SCRIPT_PATH/tools/utils.sh"
source "$RIBS_USER_SCRIPT_PATH/../tools/ryzom_utils.sh"

source "$RIBS_BASE_PATH/data/ryzom_prefs.sh"
prefs="$RIBS_BASE_PATH/data/ryzom_compilation_prefs.sh"

if [[ ! "$RIBS_GROUPS" == *":dev:"* ]]
then
	exit
fi

if [[ -f "$prefs" ]]
then
	source "$prefs"
else
	p_error "Compilation of client need be setuped before"
	p_info "Please use Recipe : Setup Shard and Ribs : Setup Compilation"
	exit
fi

if [[ -z "$*" ]]
then
	steam=""
	windows=""
	i386=""
	i64=""
	if [ "$USE_STEAM" == "1" ]
	then
		steam="Steam"
	fi
	if [ "$USE_WINE" == "1" ]
	then
		windows="+Windows"
	fi
	if [ "$USE_I386" == "1" ]
	then
		i386="32"
		i64="+64"
	fi

	ribs_script "build/ryzom_build_client.sh" "Build Client" $steam "Dev" "+Linux" "Macos" $windows $i386 $i64
	end_prepare
fi

if ribs_prepare $1
then
	p_title "Cheking repositories"
	check_git_repository $RYZOMCORE_PATH UpdateClientRepo
	p_hr
	if [ "$USE_CHROOT" == "1" ]
	then
		p_info "Compilation will use a chrooted environement"
	fi

	LABEL=""

	if [ "${OPTIONS[Linux]}" == "1" ]
	then
		LABEL="$LABEL Linux"
	fi

	if [ "${OPTIONS[Windows]}" == "1" ]
	then
		LABEL="$LABEL Windows"
	fi
	
		if [ "${OPTIONS[Macos]}" == "1" ]
	then
		LABEL="$LABEL Macos"
	fi

	if [ "${OPTIONS[Dev]}" == "1" ]
	then
		LABEL="$LABEL [dev]"
	elif [ "${OPTIONS[Steam]}" == "1" ]
	then
		LABEL="$LABEL [steam]"
	else
		LABEL="$LABEL [fv]"
	fi

	if [ "${OPTIONS[32]}" == "1" ]
	then
		LABEL="$LABEL x32"
	fi

	if [ "${OPTIONS[64]}" == "1" ]
	then
		LABEL="$LABEL x64"
	fi

	p_info "System will compile for$LABEL"

	end_prepare
fi

#Update repositories
if [ "${OPTIONS[UpdateClientRepo]}" == "1" ]
then
	cd $RYZOMCORE_PATH
	git pull
fi

function compile_clients()
{
	MODE=$1
	if [ "${OPTIONS[32]}" == "1" ]
	then
		#if [ "${OPTIONS[Linux]}" == "1" ]
		#then
		#	bash $RIBS_USER_SCRIPT_PATH/build/build_client.sh x86 GNU/Linux linux $MODE client
		#fi

		if [ "${OPTIONS[Windows]}" == "1" ]
		then
			bash $RIBS_USER_SCRIPT_PATH/build/build_client.sh x86 Windows win $MODE client
		fi
	fi

	if [ "${OPTIONS[64]}" == "1" ] || [ "$USE_I386" == "0" ]
	then
		if [ "${OPTIONS[Linux]}" == "1" ]
		then
			bash $RIBS_USER_SCRIPT_PATH/build/build_client.sh amd64 GNU/Linux linux $MODE client
		fi


		if [ "${OPTIONS[Macos]}" == "1" ]
		then
			bash $RIBS_USER_SCRIPT_PATH/build/build_client.sh x64 Macos darwin $MODE client
		fi
		
		
		if [ "${OPTIONS[Windows]}" == "1" ]
		then
			bash $RIBS_USER_SCRIPT_PATH/build/build_client.sh x64 Windows win $MODE client
		fi
	fi
}

if [ "${OPTIONS[Steam]}" == "1" ]
then
	compile_clients "steam"
fi

if [ "${OPTIONS[Dev]}" == "1" ]
then
	compile_clients "dev"
else
	compile_clients "fv"
fi


