#!/bin/sh
source "$RIBS_SCRIPT_PATH/tools/utils.sh"
source "$RIBS_USER_SCRIPT_PATH/../tools/ryzom_utils.sh"

source "$RIBS_BASE_PATH/data/ryzom_prefs.sh"
prefs="$RIBS_BASE_PATH/data/ryzom_compilation_prefs.sh"

PROC=$(nproc)

if [[ ! "$RIBS_GROUPS" == *":dev:"* ]]
then
	exit
fi

if [[ -f "$prefs" ]]
then
	source "$prefs"
else
	clr_red "Compilation of client need be setuped before"
	clr_brown "Please use Recipe : Setup Shard and Ribs : Setup Compilation"
	exit
fi

if [[ -z "$*" ]]
then
	ribs_script "build/ryzom_build_studio.sh" "Build Studio"
	end_prepare
fi

if ribs_prepare $1
then
	cd $RYZOMCORE_PATH
	check_repository $RYZOMCORE_PATH UpdateClientRepo

	if [ "$USE_CHROOT" == "1" ]
	then
		clr_green "Compilation will use a chrooted environement"
	else
		clr_white "Compilation will use own linux environement"
	fi
	
	end_prepare
fi

#Update repositories
if [ "${OPTIONS[UpdateClientRepo]}" == "1" ]
then
	cd $RYZOMCORE_PATH
	hg pull
	hg -v update
fi

#CMAKEFLAGS="-DCMAKE_BUILD_TYPE=Release -DFINAL_VERSION=1 -DWITH_RYZOM_SERVER=OFF -DWITH_RYZOM_TOOLS=OFF -DWITH_RYZOM_CLIENT=OFF -DWITH_RYZOM_INSTALLER=OFF -DWITH_RYZOM_PATCH=OFF -DWITH_NEL_TESTS=OFF -DWITH_NEL_TOOLS=OFF -DWITH_TOOLS=OFF -DWITH_NEL_SAMPLES=OFF -DWITH_ASSIMP=OFF -DWITH_WARNINGS=OFF -DWITH_LUA53=ON -DWITH_LUA51=OFF -DWITH_LIBOVR=OFF -DWITH_QT5=OFF -DWITH_RYZOM_STEAM=OFF -DWITH_INSTALL_LIBRARIES=OFF"
#CMAKEFLAGS="$CMAKEFLAGS -DOpenGL_GL_PREFERENCE=LEGACY -DQTDIR=/usr/local/Qt-$QTVERSION -DWITH_STATIC_LIBXML2=ON -DWITH_STATIC=OFF -DWITH_STATIC_DRIVERS=OFF -DWITH_STATIC_EXTERNAL=ON -DWITH_UNIX_STRUCTURE=OFF -DWITH_PCH=OFF"

CMAKEFLAGS="-DCMAKE_BUILD_TYPE=Release -DFINAL_VERSION=1 -DWITH_RYZOM_SERVER=OFF -DWITH_RYZOM_TOOLS=OFF -DWITH_RYZOM_CLIENT=OFF -DWITH_RYZOM_INSTALLER=OFF -DWITH_RYZOM_PATCH=OFF -DWITH_NEL_TESTS=OFF -DWITH_NEL_TOOLS=OFF -DWITH_TOOLS=OFF -DWITH_NEL_SAMPLES=OFF -DWITH_ASSIMP=OFF -DWITH_WARNINGS=OFF -DWITH_LUA53=OFF -DWITH_LUA51=OFF -DWITH_LIBOVR=OFF -DWITH_RYZOM_STEAM=OFF -DWITH_INSTALL_LIBRARIES=OFF -DWITH_UNIX_STRUCTURE=OFF -DWITH_PCH=OFF -DWITH_STUDIO=ON -DWITH_QT=ON -DWITH_QT5=OFF -DOpenGL_GL_PREFERENCE=LEGACY"

LABEL="Ryzom Studio"
DIR=$BUILD_PATH"/studio/"
echo "--- $DIR"
mkdir -p $DIR
cd $DIR

clr_green "Configuring $LABEL..."

if [ "$USE_CHROOT" == "1" ]
then
	CHROOT=steam64
	SESSION_FILE="$RIBS_USER_PATH/data/build_client_chroot.session"
	CHROOT_SESSION=$(schroot --begin-session -c $CHROOT)
	echo "$CHROOT_SESSION" > "$SESSION_FILE"
	schroot --run-session -c "$CHROOT_SESSION" -- cmake $RYZOMCORE_PATH/code $CMAKEFLAGS -DCMAKE_INSTALL_PREFIX=$INSTALLDIR
else
	cmake $RYZOMCORE_PATH/code $CMAKEFLAGS -DCMAKE_INSTALL_PREFIX=$INSTALLDIR
fi

check_success "$CHROOT_SESSION"
 
make -j$PROC
check_success

cp $DIR/bin/* ~/bin/
