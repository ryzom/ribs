#!/bin/bash

source "$RIBS_SCRIPT_PATH/tools/utils.sh"
source "$RIBS_USER_SCRIPT_PATH/../tools/ryzom_utils.sh"
source "$RIBS_BASE_PATH/data/ryzom_prefs.sh"
source "$RIBS_BASE_PATH/data/ryzom_compilation_prefs.sh"

ARCH=$1
SYSTEM=$2
SYSTEMSUFFIX=$3
TYPE=$4
PACKAGE=$5

FINAL_VERSION=ON
RYZOM_PATCH=ON
RYZOM_STEAM=OFF
STATIC_EXTERNAL=ON

# Windows Stuff
WINCOMPILER=vc142
WINCOMPILERVERSION=142930133

# Linux Stuff
CLIENT_ICON=ryzom_client
QT_LINUX_VERSION=5

PROC=$(nproc)
TOOLS=OFF

CHROOTPREFIX=steam

if [[ "$ARCH" = "amd64" ]] ||  [[ "$ARCH" = "x64" ]]
then
	ARCHSUFFIX=64
	if [ "$SYSTEMSUFFIX" == "linux" ]
	then
		if [ "$TYPE" != "dev" ]
		then
			p_info "Compile TOOLS"
			TOOLS=ON
		fi
	fi
else
	ARCHSUFFIX=32
fi
WITH_QT5=OFF

if [ "$TYPE" == "steam" ]
then
	USE_CHROOT=1
	RYZOM_STEAM=ON
	RYZOM_PATCH=OFF
	STATIC_EXTERNAL=ON
	WITH_QT5=OFF
	#QTVERSION="5.6.2"
	CLIENT_ICON=steam_icon_373720
	export STEAM_DIR=/usr/local/
fi

if [ "$TYPE" == "dev" ]
then
	FINAL_VERSION=OFF
fi

cd $RYZOMCORE_PATH/ryzom/client/src
REVISION=$(git rev-list HEAD --count .)
cd $RYZOMCORE_PATH
COMMIT=$(git rev-list --abbrev-commit HEAD -n 1)
BRANCH=$(git rev-parse --abbrev-ref HEAD)

if [ "$BRANCH" == "main/yubo-dev" ]
then
	DOMAIN="Alpha /"
	SHARD="yubo"
elif [ "$BRANCH" == "main/gingo-test" ]
then
	DOMAIN="Beta /"
	SHARD="gingo"
else
	DOMAIN="Omega /"
	SHARD="atys"
fi

YEAR=$(date +%y)
MONTH=$(date +%m)
DAY=$(date +%d)

LABEL="$PACKAGE $TYPE $ARCH - $SYSTEM"
DESCRIBE="$DOMAIN v$YEAR.$MONTH.$REVISION #$COMMIT"

CMAKEFLAGS="-DRYZOM_VERSION_MAJOR=$YEAR \
	-DRYZOM_VERSION_MINOR=$MONTH \
	-DRYZOM_VERSION_PATCH=$REVISION \
	-DREVISION=$REVISION \
	-DCMAKE_INSTALL_PREFIX=$INSTALLDIR \
	-DCMAKE_BUILD_TYPE=Release \
	-DFINAL_VERSION=$FINAL_VERSION \
	-DWITH_STATIC_EXTERNAL=$STATIC_EXTERNAL \
	-DWITH_RYZOM_STEAM=$RYZOM_STEAM \
	-DWITH_RYZOM_PATCH=$RYZOM_PATCH \
	-DWITH_RYZOM_TOOLS=$TOOLS \
	-DWITH_NEL_TOOLS=$TOOLS \
	-DWITH_DRIVER_OPENGL=ON \
	-DWITH_DRIVER_OPENAL=ON \
	-DWITH_TOOLS=$TOOLS \
	-DWITH_INSTALL_LIBRARIES=OFF \
	-DWITH_RYZOM_INSTALLER=OFF \
	-DWITH_RYZOM_SERVER=OFF \
	-DWITH_NEL_SAMPLES=OFF \
	-DWITH_NEL_TESTS=OFF \
	-DWITH_WARNINGS=OFF \
	-DWITH_RYZOM_CLIENT=ON \
	-DWITH_RYZOM_LIVE=ON \
	-DWITH_STATIC=ON \
	-DWITH_STATIC_DRIVERS=ON \
	-DWITH_PCH=OFF \
	-DWITH_LUA51=OFF \
	-DWITH_LUA53=ON \
	"

#	-DWITH_UNIX_STRUCTURE=OFF \
#	-DWITH_ASSIMP=OFF \
#	-DWITH_LIBOVR=OFF \
#	-DWITH_LUA51=OFF \
#	-DWITH_LUA53=ON"
#	-DOPENSSL_USE_STATIC_LIBS=ON \
#	-DWITH_STATIC_CURL=ON \



if [ "$SYSTEMSUFFIX" == "linux" ]
then
	CMAKEFLAGS="$CMAKEFLAGS \
	-DOpenGL_GL_PREFERENCE=LEGACY \
	-DOPENSSL_INCLUDE_DIR=/usr/local/ssl/include \
	-DOPENSSL_SSL_LIBRARY=/usr/local/ssl/lib64/libssl.a \
	-DOPENSSL_CRYPTO_LIBRARY=/usr/local/ssl/lib64/libcrypto.a \
	-DLUABIND_INCLUDE_DIR=/usr/include/luabind \
	-DQTDIR=/usr/local/Qt-$QTVERSION
	-DRYZOM_CLIENT_ICON=$CLIENT_ICON \
	-DWITH_SYMBOLS=ON \
	-DAL_LIBTYPE_STATIC=ON \
	-DCURL_INCLUDE_DIR=/usr/local/include/ \
	-DCURL_NO_CURL_CMAKE=ON \
	-DWITH_QT5=$WITH_QT5"
elif  [ "$SYSTEMSUFFIX" == "darwin" ]
then
	CMAKEFLAGS="$CMAKEFLAGS \
	-DOSX_SDK=10.14"
else
	CMAKEFLAGS="$CMAKEFLAGS \
	-DHUNTER_ENABLED=ON \
	-DHUNTER_CONFIGURATION_TYPES=Release \
	-DHUNTER_STATUS_DEBUG=OFF \
	-DCMAKE_CONFIGURATION_TYPES=Release;Debug \
	-DWITH_DRIVER_DIRECT3D=ON \
	-DWITH_SYMBOLS=ON \
	-DWITH_DRIVER_MINIAUDIO=OFF \
	-DWITH_DRIVER_XAUDIO2=ON"

	source $RIBS_USER_PATH/tools/wine_setenv.sh $WINCOMPILER $WINCOMPILERVERSION $ARCH
fi

INSTALLDIR=$BUILD_PATH"/$PACKAGE/binaries/"$SYSTEMSUFFIX$ARCHSUFFIX"_"$TYPE
DIR=$BUILD_PATH"/$PACKAGE/"$SYSTEMSUFFIX$ARCHSUFFIX"_"$TYPE

RYZOMCORE_CODE_PATH=$RYZOMCORE_PATH

p_title "Starting compilation [$LABEL]"
mkdir -p $DIR
cd $DIR

CHROOT_SESSION=""

if [ "$USE_CHROOT" == "1" ]
then
	CHROOT=$CHROOTPREFIX$ARCHSUFFIX
	p_info "Using chroot : $(clr_cyan $CHROOT)"

fi



p_action "⚙️ Configuring $DESCRIBE..."

if [ "$SYSTEMSUFFIX" == "linux" ]
then
	if [ "$USE_CHROOT" == "1" ]
	then
		CHROOT_SESSION=$(schroot --begin-session -c $CHROOT)
		schroot --run-session -c "$CHROOT_SESSION" -- cmake $RYZOMCORE_CODE_PATH $CMAKEFLAGS -DDESCRIBE="$DESCRIBE"
	else
		cmake $RYZOMCORE_CODE_PATH $CMAKEFLAGS -DDESCRIBE="$DESCRIBE"
	fi
elif [ "$SYSTEMSUFFIX" == "darwin" ]
then
	BUILD=$(echo $DIR | sed s/home/Users/g)-${SHARD}
	CODE=$(echo ${RYZOMCORE_CODE_PATH}-${SHARD} | sed s/home/Users/g)
	ssh -p $MACOS_SSH_SERVER "cd $BUILD/bin; rm *.7z; rm Ryzom.app/Contents/MacOS/Ryzom"
	p_action "📥 Pulling git repository... ssh -p $MACOS_SSH_SERVER cd $CODE; git pull"
	ssh -p $MACOS_SSH_SERVER "cd $CODE; git pull"
	p_action "⚙️ Running cmake into $BUILD..."
	ssh -p $MACOS_SSH_SERVER "export MACOSX_DEPLOYMENT_TARGET=10.8; export STEAM_DIR=~/steam-sdk/; export PATH=\"/opt/local/bin:/opt/local/sbin:\$PATH\"; mkdir -p $BUILD; cd $BUILD; CXXFLAGS=\"-stdlib=libc++ -Wno-everything\" cmake $CODE $CMAKEFLAGS -DDESCRIBE='$DESCRIBE'"
else
	export HUNTER_ROOT="$BUILD_PATH/hunter"
	echo "wine cmake.exe Z:$RYZOMCORE_CODE_PATH $CMAKEFLAGS  -DDESCRIBE=$DESCRIBE -G NMake Makefiles JOM"
	$WINEBIN cmake.exe "Z:$RYZOMCORE_CODE_PATH" $CMAKEFLAGS  -DDESCRIBE="$DESCRIBE" -G "NMake Makefiles JOM" -Wno-dev
fi
check_success "$CHROOT_SESSION"

p_action "📥 Compiling $DESCRIBE..."

if [ "$SYSTEMSUFFIX" == "linux" ]
then
	if [ "$USE_CHROOT" == "1" ]
	then
		CHROOT_SESSION=$(schroot --begin-session -c $CHROOT)

		schroot --run-session -c "$CHROOT_SESSION" -- make -j$PROC 2> >(
			while IFS=\n read -r line
			do
				if [[ ! -z "$line" ]] && [[ "$line" != *"no version information available"* ]]
				then
					p_error "$line"
				fi
			done) > >(
			while IFS= read line
			do
				if [[ "$line" == *"Linking"* ]]
				then
					echo "${line::7}🔗 ${line:7}"
				elif [[ "$line" == *"Built"* ]]
				then
					echo "${line::7}"$(p_ok "${line:7}")
				else
					echo "$line"
				fi
			done) < /dev/null
	else
		make -j$PROC
	fi
	for a in bin/*
	do
		if [ $a != "bin/ryzom_client" ] && [ $a != "bin/ryzom_client_patcher" ]
		then
			strip $a
			echo "♨️  Copying $a to ~/bin..."
			rsync -au $a ~/$a
		fi
	done
elif [ "$SYSTEMSUFFIX" == "darwin" ]
then
	BUILD=$(echo $DIR | sed s/home/Users/g)-${SHARD}
	CODE=$(echo ${RYZOMCORE_CODE_PATH}-${SHARD} | sed s/home/Users/g)
	ssh -p $MACOS_SSH_SERVER "export STEAM_DIR=/Users/nevrax/steam_sdk; export MACOSX_DEPLOYMENT_TARGET=10.8; export PATH=\"/opt/local/bin:/opt/local/sbin:\$PATH\"; cd $BUILD; CXXFLAGS='-Wno-inconsistent-missing-override -stdlib=libc++ -isysroot /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk' make -j4"

	if [ "$TYPE" == "steam" ]
	then
		FILENAME="ryzom_steam_osx_${REVISION}.7z"
	else
		FILENAME="ryzom_static_osx_${REVISION}.7z"
	fi
	p_action "📥 Compressing $FILENAME..."
	ssh -p $MACOS_SSH_SERVER "export PATH=\"/opt/local/bin:/opt/local/sbin:\$PATH\"; cd $BUILD/bin; rm -f $FILENAME; 7z a -mx=9 -r $FILENAME Ryzom.app"
	p_action "📥 Downloading $FILENAME..."
	scp -C -P $MACOS_SSH_SERVER:$BUILD/bin/$FILENAME $BUILD_PATH/$PACKAGE
else
	$WINEBIN jom.exe |
	while IFS=\n read -r line
	do
		line=$(echo $line | sed 's/ *$//g')
		if [[ ! -z "$line" ]] && [[ $line != *"Remarque"* ]] && [[ $line != *"Microsoft (R) Windows (R)"* ]] && [[ $line != *"Copyright (C) Microsoft Corporation."* ]]
		then
			if [[ "$RIBS_CONTEXT" == "WEB" ]]
			then
				echo ${line::-1} | sed -r  "s/\x1B\[35m/<span style='color: white'>/g" | sed -r "s/\x1B\[32m/<span style='color: orange'>/g" | sed -r "s/\x1B\[0m/<\/span>/g" | sed -r "s/\x1B\[([0-9]{1,2}(;[0-9]{1,2})?)?[mGK]//g"
			else
				echo ${line::-1}
			fi
		fi
	done
fi

echo "OK"
check_success "$CHROOT_SESSION"
echo "DONE"
exit 0
