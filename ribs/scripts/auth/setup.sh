#############################################
# | ___ \|_   _|| ___ \/  ___|
# | |_/ /  | |  | |_/ /\ `--.
# |    /   | |  | ___ \ `--. \
# | |\ \ __| |__| |_/ //\__/ /
# \_| \_(_)___(_)____(_)____(_)
#
# Remote Interface to Bash Scripts
# - better with barcebue sauce -
# Copyright (C) 2019 Nuneo (ulukyn@gmail.com)
#
# This program is free software (GPLv3): read https://www.gnu.org/licenses/gpl-3.0.en.html for more details
#
# Setup recipe script to select authentication method
#

source "$RIBS_SCRIPT_PATH/tools/utils.sh"

prefs="$RIBS_BASE_PATH/data/auth_prefs.sh"
mkdir -p "$RIBS_BASE_PATH/data/"

if [[ -z "$*" ]]
then
	ribs_script "auth/setup.sh" "Setup Authentication Method"
	end_prepare
fi

if ribs_prepare $1
then
	clr_brown "R.I.B.S need an authentication method."
	clr_brown "If you choose the method 'None', everyone are admin. So only use them in your local server"
	ask "What method do you want use?"
	methods=""
	cd "$RIBS_USER_SCRIPT_PATH"/auth/setup
	for method in *
	do
		method=$(basename $method .sh)
		if [[ ! -z "$method" ]]
		then
			methods="$method:${method^} $methods"
		fi
	done

	select_list $methods

	end_prepare
fi

cd "$RIBS_USER_SCRIPT_PATH"/auth/setup
for method in *
do
	method=$(basename $method .sh)
	if [ "${OPTIONS[$method]}" == "1" ]
	then
		echo "AUTH_METHOD=\"$method\"" > $prefs
		echo "AUTH_NEED_SETUP=1" >> $prefs
		break
	fi
done

refresh
