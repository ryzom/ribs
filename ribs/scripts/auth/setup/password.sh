#############################################
# | ___ \|_   _|| ___ \/  ___|
# | |_/ /  | |  | |_/ /\ `--.
# |    /   | |  | ___ \ `--. \
# | |\ \ __| |__| |_/ //\__/ /
# \_| \_(_)___(_)____(_)____(_)
#
# Remote Interface to Bash Scripts
# - better with barcebue sauce -
# Copyright (C) 2019 Nuneo (ulukyn@gmail.com)
#
# This program is free software (GPLv3): read https://www.gnu.org/licenses/gpl-3.0.en.html for more details
#
# Script to setup authentitcation to password method
#

source "$RIBS_SCRIPT_PATH/tools/utils.sh"

prefs="$RIBS_BASE_PATH/data/auth_prefs.sh"

if [[ -z "$*" ]]
then
	ribs_script "auth/setup/password.sh" "Setup Password Auth"
	end_prepare
fi

if ribs_prepare $1
then
	ask "Please enter the password to use:"
	enter_password Password

	end_prepare
fi

Password=${OPTIONS[Password]}

echo "AUTH_METHOD=\"password\"" > $prefs
echo "AUTH_PASSWORD=\"$Password\"" >> $prefs
refresh

