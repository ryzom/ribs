#############################################
# | ___ \|_   _|| ___ \/  ___|
# | |_/ /  | |  | |_/ /\ `--.
# |    /   | |  | ___ \ `--. \
# | |\ \ __| |__| |_/ //\__/ /
# \_| \_(_)___(_)____(_)____(_)
#
# Remote Interface to Bash Scripts
# - better with barcebue sauce -
# Copyright (C) 2019 Nuneo (ulukyn@gmail.com)
#
# This program is free software (GPLv3): read https://www.gnu.org/licenses/gpl-3.0.en.html for more details
#
# Script to setup authentitcation to none method
#

source "$RIBS_SCRIPT_PATH/tools/utils.sh"

prefs="$RIBS_BASE_PATH/data/auth_prefs.sh"

if [[ -z "$*" ]]
then
	ribs_script "auth/setup/none.sh" "Setup No Auth"
	end_prepare
fi

if ribs_prepare $1
then
	ask "Are you sure to disable authentication? (Only use it on localhost or with firewall restricted access)"
	select_list "sure:Yes, i don't need authentication" "forget:No... i need authentication, cancel the process"

	end_prepare
fi

if [ "${OPTIONS[forget]}" == "1" ]
then
	rm "$prefs"
	refresh
	exit
fi

echo "AUTH_METHOD=\"none\"" > "$prefs"

refresh
