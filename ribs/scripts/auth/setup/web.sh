#############################################
# | ___ \|_   _|| ___ \/  ___|
# | |_/ /  | |  | |_/ /\ `--.
# |    /   | |  | ___ \ `--. \
# | |\ \ __| |__| |_/ //\__/ /
# \_| \_(_)___(_)____(_)____(_)
#
# Remote Interface to Bash Scripts
# - better with barcebue sauce -
# Copyright (C) 2019 Nuneo (ulukyn@gmail.com)
#
# This program is free software (GPLv3): read https://www.gnu.org/licenses/gpl-3.0.en.html for more details
#
# Script to setup authentitcation to web method
#

source "$RIBS_SCRIPT_PATH/tools/utils.sh"

prefs=$RIBS_BASE_PATH/data/auth_prefs.sh

if [[ -z "$*" ]]
then
	ribs_script "auth/setup/web.sh" "Setup Web Authentication"
	end_prepare
fi


if ribs_prepare $1
then
	ask "Please enter the url of the script who authenticate users:"
	enter_text Url

	ask "What GET parameter use to send Login?"
	enter_text Login

	ask "What GET parametre use to send Password?"
	enter_text Password

	end_prepare
fi

Url=${OPTIONS[Url]}
Login=${OPTIONS[Login]}
Password=${OPTIONS[Password]}

echo "AUTH_METHOD=\"web\"" > $prefs
echo "WEB_AUTH_URL=\"$Url\"" >> $prefs
echo "WEB_AUTH_LOGIN=\"$Login\"" >> $prefs
echo "WEB_AUTH_PASSWORD=\"$Password\"" >> $prefs
refresh


