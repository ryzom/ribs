#############################################
# | ___ \|_   _|| ___ \/  ___|
# | |_/ /  | |  | |_/ /\ `--.
# |    /   | |  | ___ \ `--. \
# | |\ \ __| |__| |_/ //\__/ /
# \_| \_(_)___(_)____(_)____(_)
#
# Remote Interface to Bash Scripts
# - better with barcebue sauce -
# Copyright (C) 2019 Nuneo (ulukyn@gmail.com)
#
# This program is free software (GPLv3): read https://www.gnu.org/licenses/gpl-3.0.en.html for more details
#
# Script to authenticate user with password
#

source "$RIBS_SCRIPT_PATH/tools/utils.sh"

prefs="$RIBS_BASE_PATH/data/auth_prefs.sh"

if [[ -z "$*" ]]
then
	. $RIBS_SCRIPT_PATH/data/welcome.txt
	ribs_recipe "-Auth" $(ribs_script "000_init.sh" "Auth")

	clr_brown "<br /><br />Please enter the password:<br />"

	ask "Password:"
	enter_password Password

	end_prepare
fi

. $prefs
Password=${OPTIONS[Password]}

if [[ "$Password" == "$AUTH_PASSWORD" ]]
then
	login '{"success": true, "error_message": "", "username": "Owner", "priv": ":ADMIN:DEV:", "groups": ":admin:dev:"}'
else
	clr_red "Bad Password..."
	sleep 3
	refresh
fi
