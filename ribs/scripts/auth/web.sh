#############################################
# | ___ \|_   _|| ___ \/  ___|
# | |_/ /  | |  | |_/ /\ `--.
# |    /   | |  | ___ \ `--. \
# | |\ \ __| |__| |_/ //\__/ /
# \_| \_(_)___(_)____(_)____(_)
#
# Remote Interface to Bash Scripts
# - better with barcebue sauce -
# Copyright (C) 2019 Nuneo (ulukyn@gmail.com)
#
# This program is free software (GPLv3): read https://www.gnu.org/licenses/gpl-3.0.en.html for more details
#
# Script to authenticate user based on web using curl
#


source "$RIBS_SCRIPT_PATH/tools/utils.sh"

prefs="$RIBS_BASE_PATH/data/auth_prefs.sh"

if [[ -z "$*" ]]
then
	. $RIBS_SCRIPT_PATH/data/welcome.txt
	ribs_recipe "" "-Auth" $(ribs_script "000_init.sh" "Auth")

	clr_brown "<br /><br />Please enter your credentials:<br />"

	ask "Login:"
	enter_text Login

	ask "Password:"
	enter_password Password

	end_prepare
fi

. $prefs
Login=$(sanitize "${OPTIONS[Login]}")
Password=$(sanitize "${OPTIONS[Password]}")

cd $RIBS_BASE_PATH/server/
# Decrypt password using python
UrlEncoded=$(env/bin/python -c "from libs.utils import decode; from urllib.parse import urlencode; print(urlencode({'$WEB_AUTH_LOGIN': '$Login', '$WEB_AUTH_PASSWORD' : decode('$WEB_AUTH_SALT', '$Password')}))" 2>/dev/null)
AUTH_OUTPUT=$(curl -s "$WEB_AUTH_URL?$UrlEncoded")
login "$AUTH_OUTPUT"
