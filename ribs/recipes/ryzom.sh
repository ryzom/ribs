#!/bin/bash
#############################################
# | ___ \|_   _|| ___ \/  ___|
# | |_/ /  | |  | |_/ /\ `--.
# |    /   | |  | ___ \ `--. \
# | |\ \ __| |__| |_/ //\__/ /
# \_| \_(_)___(_)____(_)____(_)
#
# Remote Interface to Bash Scripts
# - better with barcebue sauce -
# Copyright (C) 2019 Nuneo (ulukyn@gmail.com)
#
# This program is free software (GPLv3): read https://www.gnu.org/licenses/gpl-3.0.en.html for more details
#
# Ribs Ryzom recipe listing
#

source "$RIBS_SCRIPT_PATH/tools/utils.sh"

prefs="$RIBS_DATA_PATH/auth_prefs.sh"
ryzom_prefs="$RIBS_DATA_PATH/ryzom_prefs.sh"

if [ ! -f $ryzom_prefs ]
then
	# Do All Checks here
	bash ryzom_shard/shard_setup.sh
	exit
fi

if [ -f $prefs ]
then
	if [ -z "$RIBS_GROUPS" ]
	then
		clr_red "No soup for you"
	else
		get_recipe ryzom_shard/shard_setup.sh
		get_recipe ryzom_shard/shard_manager.sh
		get_recipe ryzom_shard/change_shard.sh
		get_recipe ryzom_shard/build_server.sh
		get_recipe ryzom_shard/build_client.sh
		get_recipe ryzom_shard/build_tools.sh
		get_recipe ryzom_shard/patch_client.sh
		if [[ "$SHARD_TYPE" == "test" ]]
		then
			get_recipe ryzom_shard/patch_live_client.sh
		fi
		if [ "$RIBS_CONTEXT" != "WEB" ]
		then
			clr_green "Please select the recipe:"
			read recipe
			echo ${RECIPE_IDS[$recipe]}
		fi
	fi
fi
