#!/bin/bash
#############################################
# | ___ \|_   _|| ___ \/  ___|
# | |_/ /  | |  | |_/ /\ `--.
# |    /   | |  | ___ \ `--. \
# | |\ \ __| |__| |_/ //\__/ /
# \_| \_(_)___(_)____(_)____(_)
#
# Remote Interface to Bash Scripts
# - better with barcebue sauce -
# Copyright (C) 2019 Nuneo (ulukyn@gmail.com)
#
# This program is free software (GPLv3): read https://www.gnu.org/licenses/gpl-3.0.en.html for more details
#
# Ribs Authentication recipe
#

source "$RIBS_SCRIPT_PATH/tools/utils.sh"

prefs="$RIBS_BASE_PATH/data/auth_prefs.sh"

if [ ! -f $prefs ]
then
	clr_red "No authentication method..."
	clr_green "1) Please click in [Auth] button under Incredible Recipes"
	clr_green "2) Click on Prepare recipe and anwser"
	clr_green "3) Validate by Cooking recipe"

	AUTH=$(bash $RIBS_USER_SCRIPT_PATH/auth/setup.sh)
	ribs_recipe "💳" "Auth" "$AUTH"
else
	. $prefs

	if [ "$AUTH_NEED_SETUP" == "1" ]
	then
		clr_brown "Selected auth method : ${AUTH_METHOD^}"
		clr_red "The method need be setuped."
		clr_green "1) Please click in [Auth] button under Incredible Recipes"
		clr_green "2) Click on Prepare recipe and anwser"
		clr_green "3) Validate by Cooking recipe"

		AUTH=$(bash  $RIBS_USER_SCRIPT_PATH/auth/setup/$AUTH_METHOD.sh)
		ribs_recipe "💳" "Auth" "$AUTH"
	else
		bash "$RIBS_USER_SCRIPT_PATH/auth/$AUTH_METHOD.sh" $*
	fi
fi
