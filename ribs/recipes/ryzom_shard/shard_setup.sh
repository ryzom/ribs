#!/bin/bash
#############################################
# | ___ \|_   _|| ___ \/  ___|
# | |_/ /  | |  | |_/ /\ `--.
# |    /   | |  | ___ \ `--. \
# | |\ \ __| |__| |_/ //\__/ /
# \_| \_(_)___(_)____(_)____(_)
#
# Remote Interface to Bash Scripts
# - better with barcebue sauce -
# Copyright (C) 2019 Nuneo (ulukyn@gmail.com)
#
# This program is free software (GPLv3): read https://www.gnu.org/licenses/gpl-3.0.en.html for more details
#
# Ribs Ryzom setup recipe
#


source "$RIBS_SCRIPT_PATH/tools/utils.sh"

if [[ "$RIBS_GROUPS" == *":admin:"* ]]
then


	prefs="$RIBS_DATA_PATH/ryzom_prefs.sh"
	if [[ ! -f $prefs ]]
	then
		INIT=$(bash "$RIBS_USER_SCRIPT_PATH"/setup/ryzom_shard_prefs.sh)
		ribs_check $INIT
		ribs_recipe "🔧 " "Setup Shard" "$INIT"
	else
		INIT=""
		for setup in "$RIBS_USER_SCRIPT_PATH"/setup/*
		do
			init_setup=$(bash $setup)
			ribs_check "$init_setup"
			if [[ "$init_setup" != "skip" ]]
			then
				INIT="$INIT \"$init_setup\""
			fi
		done
		eval "ribs_recipe \"🔧\" \"Setup Shard\" $INIT"
	fi
else
	echo "No soup for you"
fi

