#!/bin/bash
#############################################
# | ___ \|_   _|| ___ \/  ___|
# | |_/ /  | |  | |_/ /\ `--.
# |    /   | |  | ___ \ `--. \
# | |\ \ __| |__| |_/ //\__/ /
# \_| \_(_)___(_)____(_)____(_)
#
# Remote Interface to Bash Scripts
# - better with barcebue sauce -
# Copyright (C) 2019 Nuneo (ulukyn@gmail.com)
#
# This program is free software (GPLv3): read https://www.gnu.org/licenses/gpl-3.0.en.html for more details
#
# Ribs Ryzom setup recipe
#


if [[ "$RIBS_GROUPS" == *":dev:"* ]]
then
	# Add
	BUILD=$(bash $RIBS_USER_SCRIPT_PATH/build/ryzom_build_client.sh)
	CLIENTDATA=$(bash $RIBS_USER_SCRIPT_PATH/data/generate_client_data.sh)
	PATCH=$(bash $RIBS_USER_SCRIPT_PATH/data/patch_client.sh)
	ribs_check "$BUILD"
	ribs_check "$CLIENTDATA"
	ribs_check "$PATCH"
	ribs_recipe "💈" "Build Client" "$BUILD" "$CLIENTDATA" "$PATCH"
else
	echo "no soup for you!"
fi
