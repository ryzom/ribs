#!/bin/bash
#############################################
# | ___ \|_   _|| ___ \/  ___|
# | |_/ /  | |  | |_/ /\ `--.
# |    /   | |  | ___ \ `--. \
# | |\ \ __| |__| |_/ //\__/ /
# \_| \_(_)___(_)____(_)____(_)
#
# Remote Interface to Bash Scripts
# - better with barcebue sauce -
# Copyright (C) 2019 Nuneo (ulukyn@gmail.com)
#
# This program is free software (GPLv3): read https://www.gnu.org/licenses/gpl-3.0.en.html for more details
#
# Ribs Ryzom setup recipe
#



if [[ "$RIBS_GROUPS" == *":dev:"* ]]
then
	# Add
	BUILD=$(bash $RIBS_USER_SCRIPT_PATH/build/ryzom_build_tools.sh)
	ribs_check "$BUILD"
	STUDIO=$(bash $RIBS_USER_SCRIPT_PATH/build/ryzom_build_studio.sh)
	ribs_check "$STUDIO"
	ribs_recipe "🔨" "Build RyTools" "$BUILD" "$STUDIO"
else
	echo "no soup for you!"
fi
