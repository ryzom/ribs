#!/bin/bash
#############################################
# | ___ \|_   _|| ___ \/  ___|
# | |_/ /  | |  | |_/ /\ `--.
# |    /   | |  | ___ \ `--. \
# | |\ \ __| |__| |_/ //\__/ /
# \_| \_(_)___(_)____(_)____(_)
#
# Remote Interface to Bash Scripts
# - better with barcebue sauce -
# Copyright (C) 2019 Nuneo (ulukyn@gmail.com)
#
# This program is free software (GPLv3): read https://www.gnu.org/licenses/gpl-3.0.en.html for more details
#
# Ribs Ryzom patch client recipe
#

source "$RIBS_BASE_PATH/data/ryzom_prefs.sh"

if ( [[ "$RIBS_GROUPS" == *":leveldesigner:"* ]] || [[ "$RIBS_GROUPS" == *":dev:"* ]] ) && [[ "$SHARD_TYPE" != "live" ]]
then
	# Add
	SHEETID=$(bash $RIBS_USER_SCRIPT_PATH/data/generate_sheet_id.sh)
	ribs_check "$SHEETID"
	CLIENTDATA=$(bash $RIBS_USER_SCRIPT_PATH/data/generate_client_data.sh)
	ribs_check "$CLIENTDATA"
	PATCH=$(bash $RIBS_USER_SCRIPT_PATH/data/patch_client.sh)
	ribs_check "$PATCH"
	ribs_recipe "⚗️ " "Patch Client" "$SHEETID" "$CLIENTDATA" "$PATCH"
else
	echo "No soup for you!"
fi
