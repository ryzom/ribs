#!/bin/bash
#############################################
# | ___ \|_   _|| ___ \/  ___|
# | |_/ /  | |  | |_/ /\ `--.
# |    /   | |  | ___ \ `--. \
# | |\ \ __| |__| |_/ //\__/ /
# \_| \_(_)___(_)____(_)____(_)
#
# Remote Interface to Bash Scripts
# - better with barcebue sauce -
# Copyright (C) 2019 Nuneo (ulukyn@gmail.com)
#
# This program is free software (GPLv3): read https://www.gnu.org/licenses/gpl-3.0.en.html for more details
#
# Ribs Ryzom change shard recipe
#

source "$RIBS_BASE_PATH/data/ryzom_prefs.sh"

if [[ "$RIBS_GROUPS" == *":gamedesigner:"* ]] && [[ "$SHARD_TYPE" != "live" ]] && [[ "$SHARD_TYPE" != "staging" ]] 
then
	RANGE=$(bash $RIBS_USER_SCRIPT_PATH/server_data/change_range_values.sh)
	ribs_check "$RANGE"
	WEIGHT=$(bash $RIBS_USER_SCRIPT_PATH/server_data/change_weight_values.sh)
	ribs_check "$WEIGHT"
	HITRATE=$(bash $RIBS_USER_SCRIPT_PATH/server_data/change_hitrate_rangeweapons.sh)
	ribs_check "$HITRATE"
	DAMAGE=$(bash $RIBS_USER_SCRIPT_PATH/server_data/change_damage_rangeweapons.sh)
	ribs_check "$DAMAGE"
	#HITRATEAMMO=$(sh ../scripts/change_hitrate_ammo.sh)
	DAMAGEAMMO=$(bash $RIBS_USER_SCRIPT_PATH/server_data/change_damage_ammo.sh)
	ribs_check "$DAMAGEAMMO"
	ribs_recipe "📝" "Shard Variables" "$WEIGHT" "$RANGE" "$HITRATE" "$DAMAGE" "$DAMAGEAMMO"
fi
