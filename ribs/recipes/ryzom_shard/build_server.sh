#!/bin/bash
#############################################
# | ___ \|_   _|| ___ \/  ___|
# | |_/ /  | |  | |_/ /\ `--.
# |    /   | |  | ___ \ `--. \
# | |\ \ __| |__| |_/ //\__/ /
# \_| \_(_)___(_)____(_)____(_)
#
# Remote Interface to Bash Scripts
# - better with barcebue sauce -
# Copyright (C) 2019 Nuneo (ulukyn@gmail.com)
#
# This program is free software (GPLv3): read https://www.gnu.org/licenses/gpl-3.0.en.html for more details
#
# Ribs Ryzom setup recipe
#



if [[ "$RIBS_GROUPS" == *":dev:"* ]]
then
	# Add
	STOP=$(bash $RIBS_USER_SCRIPT_PATH/shard/ryzom_stop_shard.sh)
	ribs_check "$STOP"
	BUILD=$(bash $RIBS_USER_SCRIPT_PATH/build/ryzom_build_server.sh)
	ribs_check "$BUILD"
	START=$(bash $RIBS_USER_SCRIPT_PATH/shard/ryzom_start_shard.sh)
	ribs_check "$START"

	ribs_recipe "⚗️ " "Build Server" "$STOP" "$BUILD" "$START"
else
	echo "No soup for you"
fi
