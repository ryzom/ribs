#!/bin/bash
#############################################
# | ___ \|_   _|| ___ \/  ___|
# | |_/ /  | |  | |_/ /\ `--.
# |    /   | |  | ___ \ `--. \
# | |\ \ __| |__| |_/ //\__/ /
# \_| \_(_)___(_)____(_)____(_)
#
# Remote Interface to Bash Scripts
# - better with barcebue sauce -
# Copyright (C) 2019 Nuneo (ulukyn@gmail.com)
#
# This program is free software (GPLv3): read https://www.gnu.org/licenses/gpl-3.0.en.html for more details
#
# Ribs Ryzom patch live client
#

source "$RIBS_SCRIPT_PATH/tools/utils.sh"
source "$RIBS_BASE_PATH/data/ryzom_prefs.sh"

if [[ "$RIBS_GROUPS" == *":dev:"* ]] && [[ "$SHARD_TYPE" == "staging" ]]
then
	CHECK_PATCH=$(bash $RIBS_USER_SCRIPT_PATH/data/check_patch_client.sh)
	ribs_check "$CHECK_PATCH"
	PATCH=$(bash $RIBS_USER_SCRIPT_PATH/data/patch_client.sh)
	ribs_check "$PATCH"
	SEND_TO_WEB=$(bash $RIBS_USER_SCRIPT_PATH/data/send_to_web.sh)
	ribs_check "$SEND_TO_WEB"
	ribs_recipe "🔥" "Patching Live" "$CHECK_PATCH" "$PATCH" "$SEND_TO_WEB"
fi
