#!/bin/bash

function check_git_repository {
	if [[ ! -d $1 ]]
	then
		p_error "Repository $1 don't exists..."
		exit 1
	else
		cd $1
		BRANCH=$(git rev-parse --abbrev-ref HEAD)
		if [[ -z "$BRANCH" ]]
		then
			p_error "Repository $1 don't works..."
			exit 1
		else

			REMOTE=$(git rev-parse @{u})
			LOCAL=$(git rev-parse HEAD)
			ID=$(git rev-list HEAD --count)

			if [ "$LOCAL" == "$REMOTE" ]
			then
				if [[ "$2" == "-" ]]
				then
					echo "OK"
				else
					p_ok "$1 [$BRANCH] $(clr_cyan "v$ID")"
				fi
			else
				if [[ "$2" == "-" ]]
				then
					echo "KO"
				else
					p_error "$1 [$BRANCH] $(clr_cyan "v$ID")"
					echo "$LOCAL <> $REMOTE" 
					ask "Repository is not up to date. Do you want update it?"
					select_list "No:No" "$2:Yes, update repository"
				fi
			fi
		fi
	fi
}


function update_git_repository {
	cd $1
	git pull
}


