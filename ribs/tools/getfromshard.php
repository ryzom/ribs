<?php

include_once(dirname(__FILE__).'/config.php');

$args = array_slice($argv, 1);
$ret = queryYuboShard('egs', implode(" ", $args));

if ($ret['status']) {
	$final = explode("\n", $ret['raw'][0]);
	array_shift($final);
	array_shift($final);
	echo implode("\n", $final);
}

