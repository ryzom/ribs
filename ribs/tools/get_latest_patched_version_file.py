#!/usr/bin/python

import os, sys

patch_live_path = sys.argv[1]
bnpname = sys.argv[2]
if len(sys.argv) > 3:
	patch = int(sys.argv[3])
else:
	patch = 0

files = os.listdir(patch_live_path)
versions = {}

last_version = 0
for bnp in files:
	filename, ext = bnp.split(".")
	if ext != "bnp":
		continue
	sname = filename.split("_")
	name = "_".join(sname[:-1])
	version = int(sname[-1])
	if bnpname == name:
		print(filename)
		if last_version == 0 and (patch == 0 or version <= patch):
			last_version = version
		else:
			if (patch == 0 or version <= patch) and (version > last_version):
				last_version = version

print patch_live_path+"/"+bnpname+("_%05d.bnp" % last_version)
